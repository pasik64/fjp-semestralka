package visitor;

import gen.GrammarBaseVisitor;
import gen.GrammarParser;
import object.EntryPoint;

public class ProgramVisitor extends GrammarBaseVisitor<EntryPoint> {
    @Override
    public EntryPoint visitEntryPoint(GrammarParser.EntryPointContext ctx) {
        EntryPoint entryPoint = new EntryPoint();
        ExpressionVisitor visitor = new ExpressionVisitor();
        int childCount = ctx.getChildCount();

        for (int i = 0; i < childCount; i++) {
            entryPoint.addExpression(visitor.visit(ctx.getChild(i)));
        }

        return entryPoint;
    }
}
