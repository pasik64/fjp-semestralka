package visitor;

import gen.GrammarBaseVisitor;
import gen.GrammarParser;
import object.*;
import object.Number;

import java.util.ArrayList;
import java.util.List;

public class ExpressionVisitor extends GrammarBaseVisitor<ExpressionAbstract> {
    private List<DefineVariable> vars = new ArrayList<DefineVariable>();
    private List<String> func = new ArrayList<String>();
    private List<DefineFunction> funcDef = new ArrayList<DefineFunction>();

    @Override
    public ExpressionAbstract visitBooleanValue(GrammarParser.BooleanValueContext ctx) {
        String text = ctx.getChild(0).getText();
        return new BooleanValue(text);
    }

    @Override
    public ExpressionAbstract visitIdentifier(GrammarParser.IdentifierContext ctx) {
        String text = ctx.getChild(0).getText();

        for (int i = 1; i < ctx.getChildCount(); i++) {
            text = text + ctx.getChild(i).getText();
        }

        return new Identifier(text);
    }

    @Override
    public ExpressionAbstract visitNumber(GrammarParser.NumberContext ctx) {
        String text = ctx.getChild(0).getText();

        for (int i = 1; i < ctx.getChildCount(); i++) {
            text = text + ctx.getChild(i).getText();
        }

        return new Number(text);
    }

    @Override
    public ExpressionAbstract visitDateType(GrammarParser.DateTypeContext ctx) {
        String text = ctx.getChild(0).getText();
        return new DateType(text);
    }

    @Override
    public ExpressionAbstract visitDateTypeFce(GrammarParser.DateTypeFceContext ctx) {
        String text = ctx.getChild(0).getText();
        return new DateTypeFce(text);
    }

    @Override
    public ExpressionAbstract visitCreate(GrammarParser.CreateContext ctx) {
        String text = ctx.getChild(0).getText();
        return new Create(text);
    }

    @Override
    public ExpressionAbstract visitAddOperator(GrammarParser.AddOperatorContext ctx) {
        String text = ctx.getChild(0).getText();
        return new AddOperator(text);
    }

    @Override
    public ExpressionAbstract visitMultiplyOperator(GrammarParser.MultiplyOperatorContext ctx) {
        String text = ctx.getChild(0).getText();
        return new MultiplyOperator(text);
    }

    @Override
    public ExpressionAbstract visitLogicalOperator(GrammarParser.LogicalOperatorContext ctx) {
        String text = ctx.getChild(0).getText();
        return new LogicalOperator(text);
    }

    @Override
    public ExpressionAbstract visitLogicalOperatorBinary(GrammarParser.LogicalOperatorBinaryContext ctx) {
        String text = ctx.getChild(0).getText();
        return new LogicalOperatorBinary(text);
    }

    @Override
    public ExpressionAbstract visitNegateLogicalOperation(GrammarParser.NegateLogicalOperationContext ctx) {
        int childCount = ctx.getChildCount();
        boolean negate = false;
        if (childCount == 2) {
            negate = true;
        }

        LogicalOperationInner operation = (LogicalOperationInner) visit(ctx.getChild(childCount - 1));
        return new NegateLogicalOperation(negate, operation);
    }

    @Override
    public ExpressionAbstract visitLogicalOperation(GrammarParser.LogicalOperationContext ctx) {
        LogicalOperation operation = new LogicalOperation();
        int childCount = ctx.getChildCount();

        NegateLogicalOperation item = (NegateLogicalOperation) visit(ctx.getChild(0));
        operation.addLogicalOperation(item);

        if (childCount > 1) {
            for (int i = 1; i < childCount; i += 2) {
                LogicalOperatorBinary operator = (LogicalOperatorBinary) visit(ctx.getChild(i));
                item = (NegateLogicalOperation) visit(ctx.getChild(i + 1));
                operation.addLogicalOperation(item, operator);
            }
        }

        return operation;
    }

    @Override
    public ExpressionAbstract visitLogicalOperationInner(GrammarParser.LogicalOperationInnerContext ctx) {
        int childCount = ctx.getChildCount();
        if (childCount == 3) {
            NumeralOperation op1 = (NumeralOperation) visit(ctx.getChild(0));
            LogicalOperator operator = (LogicalOperator) visit(ctx.getChild(1));
            NumeralOperation op2 = (NumeralOperation) visit(ctx.getChild(2));
            return new LogicalOperationInner(op1, operator, op2);
        } else {
            ExpressionAbstract item = visit(ctx.getChild(0));
            if (item instanceof Identifier) {
                DefineVariable var = findVariable((Identifier) item);
                if (!(var.getDateType().getType().equals("boolean"))) {
                    int line = ctx.getStart().getLine();
                    String error = "Error: Variable " + var.getIdentifier().getIdentifier() + " on line " + line + " isn't boolean";
                    System.out.println(error);
                    System.exit(0);
                }

                return new LogicalOperationInner((Identifier) item);
            } else {
                return new LogicalOperationInner((BooleanValue) item);
            }
        }
    }

    @Override
    public ExpressionAbstract visitNumeralOperation(GrammarParser.NumeralOperationContext ctx) {
        int childCount = ctx.getChildCount();
        if (childCount == 1) {
            NumeralOperationInner operation = (NumeralOperationInner) visit(ctx.getChild(0));
            return new NumeralOperation(operation);
        } else {
            NumeralOperation operation0 = (NumeralOperation) visit(ctx.getChild(0));
            AddOperator operator = (AddOperator) visit(ctx.getChild(1));
            NumeralOperationInner operation1 = (NumeralOperationInner) visit(ctx.getChild(2));
            return new NumeralOperation(operation0, operator, operation1);
        }
    }

    @Override
    public ExpressionAbstract visitNumeralOperationInner(GrammarParser.NumeralOperationInnerContext ctx) {
        int childCount = ctx.getChildCount();
        if (childCount == 1) {
            NumeralOperationFinal operation = (NumeralOperationFinal) visit(ctx.getChild(0));
            return new NumeralOperationInner(operation);
        } else {
            NumeralOperationInner operation0 = (NumeralOperationInner) visit(ctx.getChild(0));
            MultiplyOperator operator = (MultiplyOperator) visit(ctx.getChild(1));
            NumeralOperationFinal operation1 = (NumeralOperationFinal) visit(ctx.getChild(2));
            return new NumeralOperationInner(operation0, operator, operation1);
        }
    }

    @Override
    public ExpressionAbstract visitNumeralOperationFinal(GrammarParser.NumeralOperationFinalContext ctx) {
        ExpressionAbstract item = visit(ctx.getChild(0));
        if (item instanceof Identifier) {
            DefineVariable var = findVariable((Identifier) item);
            if (var == null) {
                int line = ctx.getStart().getLine();
                String error = "Error: Variable " + ((Identifier) item).getIdentifier() + " on line " + line + " wasn't declared yet";
                System.out.println(error);
                System.exit(0);
            } else if (!var.getDateType().getType().equals("int")) {
                int line = ctx.getStart().getLine();
                String error = "Error: Variable " + var.getIdentifier().getIdentifier() + " on line " + line + " isn't number";
                System.out.println(error);
                System.exit(0);
            }

            NumeralOperationFinal result = new NumeralOperationFinal((Identifier) item);
            return result;
        } else if (item instanceof BracketsOperation) {
            NumeralOperationFinal result = new NumeralOperationFinal((BracketsOperation) item);
            return result;
        } else {
            NumeralOperationFinal result = new NumeralOperationFinal((Number) item);
            return result;
        }
    }

    @Override
    public ExpressionAbstract visitBracketsOperation(GrammarParser.BracketsOperationContext ctx) {
        NumeralOperation operation = (NumeralOperation) visit(ctx.getChild(1));
        return new BracketsOperation(operation);
    }

    @Override
    public ExpressionAbstract visitOperation(GrammarParser.OperationContext ctx) {
        OperationAbstract abstractOp = (OperationAbstract) visit(ctx.getChild(0));
        String type = "";
        if (abstractOp instanceof LogicalOperation) {
            type = "boolean";
        } else if (abstractOp instanceof NumeralOperation) {
            type = "int";
        } else if (abstractOp instanceof CallFunction) {
            Identifier fceIdentifier = ((CallFunction) abstractOp).getIdentifier();
            type = this.getFunctionReturnType(fceIdentifier).getType();
            if (type.equals("void")) {
                int line = ctx.getStart().getLine();
                String error = "Error: Function " + fceIdentifier.getIdentifier() + " on line " + line + " doesn't return value";
                System.out.println(error);
                System.exit(0);
            }
        } else if (abstractOp instanceof TernaryOperation) {
            type = ((TernaryOperation) abstractOp).getType();
        }

        return new Operation(abstractOp, type);
    }

    @Override
    public ExpressionAbstract visitTernaryOperation(GrammarParser.TernaryOperationContext ctx) {
        Condition condition = (Condition) visit(ctx.getChild(0));
        Operation op1 = (Operation) visit(ctx.getChild(2));
        Operation op2 = (Operation) visit(ctx.getChild(4));
        return new TernaryOperation(ctx, condition, op1, op2);
    }

    @Override
    public ExpressionAbstract visitParameters(GrammarParser.ParametersContext ctx) {
        int listLenght = ctx.getChildCount();

        Parameters parameters = new Parameters();
        if (listLenght == 3) {
            Parameter parameter = (Parameter) visit(ctx.getChild(1));
            parameters.addParameter(parameter, ctx);
        } else if (listLenght > 3) {
            Parameter parameter = (Parameter) visit(ctx.getChild(1));
            parameters.addParameter(parameter, ctx);

            int paramCount = (listLenght - 3) / 2;
            for (int i = 3, processedParam = 0; processedParam < paramCount; i += 2, processedParam++) {
                parameter = (Parameter) visit(ctx.getChild(i));
                parameters.addParameter(parameter, ctx);
            }
        }
        return parameters;
    }

    @Override
    public ExpressionAbstract visitParameter(GrammarParser.ParameterContext ctx) {
        String text = ctx.getChild(0).getText();
        DateType dateType = new DateType(text);

        text = ctx.getChild(1).getText();
        Identifier identifier = new Identifier(text);

        DefineVariable var = new DefineVariable(new Create("definuj"), dateType, identifier);
        vars.add(var);
        return new Parameter(dateType, identifier);
    }

    @Override
    public ExpressionAbstract visitCondition(GrammarParser.ConditionContext ctx) {
        LogicalOperation logicalOperation = (LogicalOperation) visit(ctx.getChild(1));
        return new Condition(logicalOperation);
    }

    @Override
    public ExpressionAbstract visitDefineVariable(GrammarParser.DefineVariableContext ctx) {
        Create create = (Create) visit(ctx.getChild(0));
        DateType dateType = (DateType) visit(ctx.getChild(1));
        int childCount = ctx.getChildCount();

        if (childCount == 6) {
            ExpressionAbstract expression = visit(ctx.getChild(2));
            if (expression instanceof Identifier) {
                //this is for normal type
                return this.createSingleDefineVariable(ctx, 2, childCount, create, dateType);
            } else {
                //this is for paralel type
                return this.createParalelDefineVariable(ctx, create, dateType);
            }
        } else {
            //this is for multi type
            return this.createMultiDefineVariable(ctx, ctx.getChildCount(), create, dateType);
        }

    }

    @Override
    public ExpressionAbstract visitDefineFunction(GrammarParser.DefineFunctionContext ctx) {
        DateTypeFce dateTypeFce = (DateTypeFce) visit(ctx.getChild(1));
        Identifier identifier = (Identifier) visit(ctx.getChild(2));
        Parameters parameters = (Parameters) visit(ctx.getChild(3));
        FunctionBody functionBody = (FunctionBody) visit(ctx.getChild(4));
        DefineFunction defineFce = new DefineFunction(dateTypeFce, identifier, parameters, functionBody);

        if (func.contains(identifier.getIdentifier())) {
            int line = ctx.getStart().getLine();
            String error = "Error: Function " + identifier.getIdentifier() + " on line " + line + " was already created before";
            System.out.println(error);
            System.exit(0);
        } else {
            List<ReturnValue> retvals = functionBody.getReturnValueList();

            if (this.checkReturn(retvals, dateTypeFce)) {
                func.add(identifier.getIdentifier());
                funcDef.add(defineFce);
            } else {
                int line = ctx.getStart().getLine();
                String error = "Error: Function " + identifier.getIdentifier() + " on line " + line + " returned different type then expected";
                System.out.println(error);
                System.exit(0);
            }
        }

        if (!dateTypeFce.getType().equals("void")) {
            List<FunctionRow> rows = functionBody.getFunctionRowList();
            if (!(rows.get(rows.size() - 1).isReturn())) {
                String error = "Error: Function " + identifier.getIdentifier() + " doesn't end by return";
                System.out.println(error);
                System.exit(0);
            }
        }

        return defineFce;
    }

    @Override
    public ExpressionAbstract visitCallFunction(GrammarParser.CallFunctionContext ctx) {
        Identifier identifier = (Identifier) visit(ctx.getChild(0));
        CallFunctionParameters callFunctionParameters = (CallFunctionParameters) visit(ctx.getChild(1));

        if (!func.contains(identifier.getIdentifier())) {
            int line = ctx.getStart().getLine();
            String error = "Error: Called function " + identifier.getIdentifier() + " on line " + line + " doesn't exist";
            System.out.println(error);
            System.exit(0);
        }

        DefineFunction defineFunction = findFunction(identifier);

        return new CallFunction(identifier, callFunctionParameters, defineFunction.getFunctionBody());
    }

    @Override
    public ExpressionAbstract visitCallFunctionParameters(GrammarParser.CallFunctionParametersContext ctx) {
        List<CallParameterAbstract> identifierList = new ArrayList<CallParameterAbstract>();

        int childrenCount = ctx.getChildCount();
        if (childrenCount == 3) {
            CallParameterAbstract item = (CallParameterAbstract) visit(ctx.getChild(1));
            identifierList.add(item);
        } else if (childrenCount > 3) {
            CallParameterAbstract item = (CallParameterAbstract) visit(ctx.getChild(1));
            identifierList.add(item);

            for (int i = 3; i < childrenCount - 1; i += 2) {
                item = (CallParameterAbstract) visit(ctx.getChild(i));
                identifierList.add(item);
            }
        }
        return new CallFunctionParameters(identifierList);
    }

    @Override
    public ExpressionAbstract visitFunctionBody(GrammarParser.FunctionBodyContext ctx) {
        int childCount = ctx.getChildCount();
        List<FunctionRow> rows = new ArrayList<FunctionRow>();
        List<ReturnValue> returns = new ArrayList<ReturnValue>();

        if (childCount > 2) {
            for (int i = 1; i < childCount - 1; i++) {
                FunctionRow row = (FunctionRow) visit(ctx.getChild(i));
                List<ReturnValue> value = row.getReturnValueList();
                if (value != null) {
                    returns.addAll(value);
                }
                rows.add(row);
            }
        }

        return new FunctionBody(rows, returns);
    }

    @Override
    public ExpressionAbstract visitFunctionRow(GrammarParser.FunctionRowContext ctx) {
        ExpressionAbstract result = visit(ctx.getChild(0));
        return new FunctionRow(result);
    }

    @Override
    public ExpressionAbstract visitIfFunction(GrammarParser.IfFunctionContext ctx) {
        Condition condition = (Condition) visit(ctx.getChild(1));
        FunctionBody body = (FunctionBody) visit(ctx.getChild(2));

        if (ctx.getChildCount() == 5) {
            FunctionBody elseBody = (FunctionBody) visit(ctx.getChild(4));
            return new IfFunction(condition, body, elseBody);
        } else {
            return new IfFunction(condition, body);
        }
    }

    @Override
    public ExpressionAbstract visitWhileFunction(GrammarParser.WhileFunctionContext ctx) {
        Condition condition = (Condition) visit(ctx.getChild(1));
        FunctionBody body = (FunctionBody) visit(ctx.getChild(2));
        return new WhileFunction(condition, body);
    }

    @Override
    public ExpressionAbstract visitUntilFunction(GrammarParser.UntilFunctionContext ctx) {
        Condition condition = (Condition) visit(ctx.getChild(1));
        FunctionBody body = (FunctionBody) visit(ctx.getChild(2));
        return new UntilFunction(condition, body);
    }

    @Override
    public ExpressionAbstract visitDoWhileFunction(GrammarParser.DoWhileFunctionContext ctx) {
        FunctionBody body = (FunctionBody) visit(ctx.getChild(1));
        Condition condition = (Condition) visit(ctx.getChild(3));
        return new DoWhileFunction(condition, body);
    }

    @Override
    public ExpressionAbstract visitDoUntilFunction(GrammarParser.DoUntilFunctionContext ctx) {
        FunctionBody body = (FunctionBody) visit(ctx.getChild(1));
        Condition condition = (Condition) visit(ctx.getChild(3));
        return new DoUntilFunction(condition, body);
    }

    @Override
    public ExpressionAbstract visitForFunction(GrammarParser.ForFunctionContext ctx) {
        Identifier initIdentifier = (Identifier) visit(ctx.getChild(2));
        NumeralOperation initOperation = (NumeralOperation) visit(ctx.getChild(4));

        if (this.findVariable(initIdentifier) != null) {
            int line = ctx.getStart().getLine();
            String error = "Error: cyklus variable " + initIdentifier.getIdentifier() + " on line " + line + " already exists";
            System.out.println(error);
            System.exit(0);
        }

        vars.add(new DefineVariable(new Create("definuj"), new DateType("int"), initIdentifier, new Operation(initOperation, "int")));

        LogicalOperation condition = (LogicalOperation) visit(ctx.getChild(6));

        Identifier identifier = (Identifier) visit(ctx.getChild(8));
        NumeralOperation operation = (NumeralOperation) visit(ctx.getChild(10));

        FunctionBody body = (FunctionBody) visit(ctx.getChild(12));


        return new ForFunction(condition, body, initIdentifier, initOperation, identifier, operation);
    }

    @Override
    public ExpressionAbstract visitReturnValue(GrammarParser.ReturnValueContext ctx) {
        if (ctx.getChild(1) instanceof GrammarParser.IdentifierContext) {
            return new ReturnValue((Identifier) visit(ctx.getChild(1)));
        } else if (ctx.getChild(1) instanceof GrammarParser.NumberContext) {
            return new ReturnValue((Number) visit(ctx.getChild(1)));
        } else if (ctx.getChild(1) instanceof GrammarParser.BooleanValueContext) {
            return new ReturnValue((BooleanValue) visit(ctx.getChild(1)));
        } else return new ReturnValue();
    }

    private DefineVariable findVariable(Identifier identifier) {
        for (DefineVariable item : vars) {
            String type = item.getVariableType();
            if (type.equals("normal")) {
                if (item.getIdentifier().getIdentifier().equals(identifier.getIdentifier())) {
                    return item;
                }
            } else {
                for (Identifier subitem : item.getIdentifierList()) {
                    if (subitem.getIdentifier().equals(identifier.getIdentifier())) {
                        return item;
                    }
                }
            }
        }
        return null;
    }

    private DefineFunction findFunction(Identifier identifier) {
        for (int i = 0; i < funcDef.size(); i++) {
            DefineFunction item = funcDef.get(i);
            if (item.getIdentifier().getIdentifier().equals(identifier.getIdentifier())) {
                return item;
            }
        }
        return null;
    }

    private DefineVariable createSingleDefineVariable(GrammarParser.DefineVariableContext ctx, int i, int childCount, Create create, DateType dateType) {
        Identifier identifier = (Identifier) visit(ctx.getChild(i));
        Operation operation = (Operation) visit(ctx.getChild(childCount - 2));
        DefineVariable variable = new DefineVariable(create, dateType, identifier, operation);

        //checks if expected and actual type are equal
        if (dateType.getType().equals(operation.getType())) {
            //checks if variable can be created/replaced
            replaceVariable(ctx, identifier, variable);
        } else {
            int line = ctx.getStart().getLine();
            String error = "Error: Declared and actual type of variable " + identifier.getIdentifier() + " on line " + line + " don't match";
            System.out.println(error);
            System.exit(0);
        }

        return variable;
    }

    private DefineVariable createMultiDefineVariable(GrammarParser.DefineVariableContext ctx, int childCount, Create create, DateType dateType) {
        List<Identifier> identifierList = new ArrayList<Identifier>();
        Operation operation = (Operation) visit(ctx.getChild(childCount - 2));

        for (int i = 2; i < childCount - 3; i += 2) {
            Identifier identifier = (Identifier) visit(ctx.getChild(i));
            identifierList.add(identifier);
        }

        DefineVariable variable = new DefineVariable(create, dateType, identifierList, operation);

        //checks if expected and actual type are equal
        if (dateType.getType().equals(operation.getType())) {
            //checks if variable can be created/replaced
            for (Identifier identifier : identifierList) {
                replaceVariable(ctx, identifier, variable);
            }
        } else {
            int line = ctx.getStart().getLine();
            String identifiers = "";
            for (int i = 0; i < identifierList.size(); i++) {
                identifiers += identifierList.get(i).getIdentifier();
                if (i == identifierList.size() - 1) {
                    identifiers += ", ";
                }
            }
            String error = "Error: Declared and actual type of variables " + identifiers + " on line " + line + " don't match";
            System.out.println(error);
            System.exit(0);
        }

        return variable;
    }

    private DefineVariable createParalelDefineVariable(GrammarParser.DefineVariableContext ctx, Create create, DateType dateType) {
        List<Identifier> identifierList = new ArrayList<Identifier>();
        List<Operation> operationList = new ArrayList<Operation>();
        GrammarParser.MultipleIdentifiersContext ctx1 = (GrammarParser.MultipleIdentifiersContext) ctx.getChild(2);
        GrammarParser.MultipleOperationsContext ctx2 = (GrammarParser.MultipleOperationsContext) ctx.getChild(4);

        for (int i = 1; i < ctx1.getChildCount() - 1; i += 2) {
            Identifier identifier = (Identifier) visit(ctx1.getChild(i));
            identifierList.add(identifier);
        }

        for (int i = 1; i < ctx2.getChildCount() - 1; i += 2) {
            Operation operation = (Operation) visit(ctx2.getChild(i));
            operationList.add(operation);
        }

        DefineVariable variable = new DefineVariable(create, dateType, identifierList, operationList);

        //checks if expected and actual types are equal
        for (Operation operation : operationList) {
            if (dateType.getType().equals(operation.getType())) {
                //checks if variable can be created/replaced
                for (Identifier identifier : identifierList) {
                    replaceVariable(ctx, identifier, variable);
                }
            } else {
                int line = ctx.getStart().getLine();
                String identifiers = "";
                for (int i = 0; i < identifierList.size(); i++) {
                    identifiers += identifierList.get(i).getIdentifier();
                    if (i == identifierList.size() - 1) {
                        identifiers += ", ";
                    }
                }
                String error = "Error: Declared and actual type of variables " + identifiers + " on line " + line + " don't match";
                System.out.println(error);
                System.exit(0);
            }
        }

        return variable;
    }

    //checks if variable can be created/replaced
    private void replaceVariable(GrammarParser.DefineVariableContext ctx, Identifier identifier, DefineVariable variable) {
        DefineVariable item = this.findVariable(identifier);
        if (item == null) {
            vars.add(variable);
        } else {
            if (item.getVariableType().equals("normal") && item.getCreate().getType().equals("definuj")) {
                vars.remove(item);
                vars.add(variable);
            } else if (item.getCreate().getType().equals("definuj")) {
                if (!item.equals(variable)) {
                    vars.remove(item);
                    DefineVariable item2;
                    if (item.getVariableType().equals("paralel")) {
                        item2 = new DefineVariable(item.getCreate(), item.getDateType(), item.subtractIdentifier(identifier), item.subtractOperation(identifier));
                    } else {
                        item2 = new DefineVariable(item.getCreate(), item.getDateType(), item.subtractIdentifier(identifier), item.getOperation());
                    }
                    vars.add(item2);
                    vars.add(variable);
                }
            } else {
                int line = ctx.getStart().getLine();
                String error = "Error: Variable " + identifier.getIdentifier() + " on line " + line + " was already created as constant";
                System.out.println(error);
                System.exit(0);
            }
        }
    }

    private DateTypeFce getFunctionReturnType(Identifier identifier) {
        for (int i = 0; i < funcDef.size(); i++) {
            DefineFunction item = funcDef.get(i);
            if (item.getIdentifier().getIdentifier().equals(identifier.getIdentifier())) {
                return item.getDateTypeFce();
            }
        }
        return null;
    }

    private boolean checkReturn(List<ReturnValue> values, DateTypeFce expected) {
        if (expected.getType().equals("void")) {
            return values.isEmpty();
        } else {
            if (values.size() < 1) {
                return false;
            }

            for (ReturnValue value : values) {
                if (!value.getType(vars).equals(expected.getType())) {
                    return false;
                }
            }
            return true;
        }
    }
}
