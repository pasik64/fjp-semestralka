package object;


/**
 * RULE
 * create : CREATE_VAR | CREATE_CONST
 */
public class Create extends ExpressionAbstract {
    private String type;

    public Create (String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }
}
