package object;

/**
 * RULE
 * logicalOperator : EQUAL | LESS | EQ_LESS | GREATER | EQ_GREATER | NOT_EQUAL
 */
public class LogicalOperator extends ExpressionAbstract {
    private String type;

    public LogicalOperator (String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }
}
