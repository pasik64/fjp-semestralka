package object;

/**
 * RULE
 * callFunction : identifier callFunctionParameters SEMICOLON
 */
public class CallFunction extends OperationAbstract {
    private Identifier identifier;
    private CallFunctionParameters callFunctionParameters;
    private FunctionBody functionBody;

    public CallFunction(Identifier identifier, CallFunctionParameters callFunctionParameters, FunctionBody functionBody) {
        this.identifier = identifier;
        this.callFunctionParameters = callFunctionParameters;
        this.functionBody = functionBody;
    }

    public Identifier getIdentifier() {
        return identifier;
    }

    public CallFunctionParameters getCallFunctionParameters() {
        return callFunctionParameters;
    }

    public FunctionBody getFunctionBody() {
        return functionBody;
    }
}
