package object;

public class MultiplyOperator extends ExpressionAbstract{
    public String type;

    public MultiplyOperator(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }
}
