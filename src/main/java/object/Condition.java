package object;

/**
 * RULE
 * condition: OPEN_BRACKET logicalOperation CLOSE_BRACKET
 */
public class Condition extends ExpressionAbstract{

    private LogicalOperation logicalOperation;

    public Condition (LogicalOperation logicalOperation) {
        this.logicalOperation = logicalOperation;
    }

    public LogicalOperation getLogicalOperation() {
        return logicalOperation;
    }
}
