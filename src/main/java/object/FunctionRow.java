package object;

import java.util.ArrayList;
import java.util.List;

/**
 * RULE
 * functionRow : defineVariable | ifFunction | whileFunction | returnValue | callFunction
 */
public class FunctionRow extends ExpressionAbstract {

    private DefineVariable defineVariable;
    private IfFunction ifFunction;
    private WhileFunction whileFunction;
    private DoWhileFunction doWhileFunction;
    private UntilFunction untilFunction;
    private DoUntilFunction doUntilFunction;
    private ForFunction forFunction;
    private CallFunction functionCall;
    private List<ReturnValue> returnValueList;
    private boolean isReturn = false;

    public FunctionRow(ExpressionAbstract expression) {
        if (expression instanceof DefineVariable) {
            this.defineVariable = (DefineVariable) expression;
            this.returnValueList = new ArrayList<ReturnValue>();
        } else if (expression instanceof IfFunction) {
            this.ifFunction = (IfFunction) expression;
            this.returnValueList = ((IfFunction) expression).getReturnValueList();
        } else if (expression instanceof WhileFunction) {
            this.whileFunction = (WhileFunction) expression;
            this.returnValueList = ((WhileFunction) expression).getReturnValueList();
        } else if (expression instanceof DoWhileFunction) {
            this.doWhileFunction = (DoWhileFunction) expression;
            this.returnValueList = ((DoWhileFunction) expression).getReturnValueList();
        } else if (expression instanceof UntilFunction) {
            this.untilFunction = (UntilFunction) expression;
            this.returnValueList = ((UntilFunction) expression).getReturnValueList();
        } else if (expression instanceof DoUntilFunction) {
            this.doUntilFunction = (DoUntilFunction) expression;
            this.returnValueList = ((DoUntilFunction) expression).getReturnValueList();
        } else if (expression instanceof ForFunction) {
            this.forFunction = (ForFunction) expression;
            this.returnValueList = ((ForFunction) expression).getReturnValueList();
        } else if (expression instanceof ReturnValue) {
            this.returnValueList = new ArrayList<ReturnValue>();
            ReturnValue value = (ReturnValue) expression;
            isReturn = true;
            if (!value.isEmpty()) {
                this.returnValueList.add(value);
            }
        } else if (expression instanceof CallFunction) {
            this.functionCall = (CallFunction) expression;
        }
    }

    public DefineVariable getDefineVariable() {
        return defineVariable;
    }

    public IfFunction getIfFunction() {
        return ifFunction;
    }

    public List<ReturnValue> getReturnValueList() {
        return returnValueList;
    }

    public WhileFunction getWhileFunction() {
        return whileFunction;
    }

    public CallFunction getFunctionCall() {
        return functionCall;
    }

    public DoWhileFunction getDoWhileFunction() {
        return doWhileFunction;
    }

    public UntilFunction getUntilFunction() {
        return untilFunction;
    }

    public DoUntilFunction getDoUntilFunction() {
        return doUntilFunction;
    }

    public ForFunction getForFunction() {
        return forFunction;
    }

    public boolean isReturn() {
        return isReturn;
    }
}
