package object;

import java.util.List;

/**
 * RULE
 * forFunction : FOR_KEYWORD OPEN_BRACKET
 * identifier ASSIGN numeralOperation SEMICOLON
 * logicalOperation SEMICOLON
 * identifier ASSIGN numeralOperation
 * CLOSE_BRACKET functionBody
 */
public class ForFunction extends ExpressionAbstract {

    private LogicalOperation condition;
    private FunctionBody functionBody;
    private List<ReturnValue> returnValueList;
    private Identifier identifier;
    private NumeralOperation operation;
    private Identifier initIdentifier;
    private NumeralOperation initOperation;

    public ForFunction(LogicalOperation condition, FunctionBody functionBody, Identifier initIdentifier, NumeralOperation initOperation, Identifier identifier, NumeralOperation operation) {
        this.condition = condition;
        this.identifier = identifier;
        this.operation = operation;
        this.initIdentifier = initIdentifier;
        this.initOperation = initOperation;
        this.functionBody = functionBody;
        this.returnValueList = functionBody.getReturnValueList();
    }

    public FunctionBody getFunctionBody() {
        return functionBody;
    }

    public LogicalOperation getCondition() {
        return condition;
    }

    public List<ReturnValue> getReturnValueList() {
        return returnValueList;
    }

    public Identifier getIdentifier() {
        return identifier;
    }

    public NumeralOperation getOperation() {
        return operation;
    }

    public NumeralOperation getInitOperation() {
        return initOperation;
    }

    public Identifier getInitIdentifier() {
        return initIdentifier;
    }
}
