package object;

import java.util.ArrayList;
import java.util.List;

/**
 * RULE
 * create dateType identifier ASSIGN (identifier ASSIGN)* operation SEMICOLON
 * | create dateType multipleIdentifiers ASSIGN multipleOperations SEMICOLON
 */
public class DefineVariable extends ExpressionAbstract {
    private String variableType;

    private Create create;
    private DateType dateType;

    //defines for normal type
    private Identifier identifier;
    private Operation operation;

    //defines for multiple
    private List<Identifier> identifierList;

    //defines for paralel
    private List<Operation> operationList;
    //+ List<Identifier> identifierList;


    //this constructor is ONLY for function parameters!
    public DefineVariable(Create create, DateType dateType, Identifier identifier) {
        this.create = create;
        this.dateType = dateType;
        this.identifier = identifier;

        this.variableType = "normal";
    }

    //this constructor is ONLY for normal declaration!
    public DefineVariable(Create create, DateType dateType, Identifier identifier, Operation operation) {
        this.create = create;
        this.dateType = dateType;
        this.identifier = identifier;
        this.operation = operation;

        this.variableType = "normal";
    }

    //this constructor is ONLY for multi declaration!
    public DefineVariable(Create create, DateType dateType, List<Identifier> identifierList, Operation operation) {
        this.create = create;
        this.dateType = dateType;
        this.identifierList = identifierList;
        this.operation = operation;

        this.variableType = "multiple";
    }

    //this constructor is ONLY for paralel declaration!
    public DefineVariable(Create create, DateType dateType, List<Identifier> identifierList, List<Operation> operationList) {
        this.create = create;
        this.dateType = dateType;
        this.identifierList = identifierList;
        this.operationList = operationList;

        this.variableType = "paralel";
    }

    public DateType getDateType() {
        return dateType;
    }

    public Identifier getIdentifier() {
        return identifier;
    }

    public Create getCreate() {
        return create;
    }

    public Operation getOperation() {
        return operation;
    }

    public List<Identifier> getIdentifierList() {
        return identifierList;
    }

    public List<Operation> getOperationList() {
        return operationList;
    }

    public List<Identifier> subtractIdentifier(Identifier item) {
        List<Identifier> newList = new ArrayList<>();
        for (int i = 0; i < identifierList.size(); i++) {
            String id = identifierList.get(i).getIdentifier();
            if (!id.equals(item.getIdentifier())) {
                newList.add(item);
            }
        }
        return newList;
    }

    public List<Operation> subtractOperation(Identifier item) {
        List<Operation> newList = new ArrayList<>();
        for (int i = 0; i < identifierList.size(); i++) {
            String id = identifierList.get(i).getIdentifier();
            if (!id.equals(item.getIdentifier())) {
                newList.add(operationList.get(i));
            }
        }
        return newList;
    }

    public String getVariableType() {
        return variableType;
    }
}
