package object;

/**
 * RULE
 * operation : logicalOperation | numeralOperation | stringDeclarationOperation | callFunction | ternaryOperation
 */
public class Operation extends ExpressionAbstract {
    private OperationAbstract operationAbstract;
    private String type;

    public Operation(OperationAbstract operationAbstract, String type) {
        this.operationAbstract = operationAbstract;
        this.type = type;
    }

    public OperationAbstract getOperationAbstract() {
        return operationAbstract;
    }

    public String getType() {
        return type;
    }
}
