package object;

import java.util.List;

/**
 * RULE
 * doWhileFunction : DO_KEYWORD functionBody WHILE_KEYWORD condition
 */
public class DoWhileFunction extends ExpressionAbstract {

    private Condition condition;
    private FunctionBody functionBody;
    private List<ReturnValue> returnValueList;

    public DoWhileFunction(Condition condition, FunctionBody functionBody) {
        this.condition = condition;
        this.functionBody = functionBody;
        this.returnValueList = functionBody.getReturnValueList();
    }

    public FunctionBody getFunctionBody() {
        return functionBody;
    }

    public Condition getCondition() {
        return condition;
    }

    public List<ReturnValue> getReturnValueList() {
        return returnValueList;
    }
}
