package object;

import gen.GrammarParser;

/**
 * RULE
 * ternaryOperation : condition QUESTION_MARK operation COLON operation
 */
public class TernaryOperation extends OperationAbstract {
    private Condition condition;
    private Operation op1;
    private Operation op2;
    private String type;

    public TernaryOperation(GrammarParser.TernaryOperationContext ctx, Condition condition, Operation op2, Operation op1) {
        this.op1 = op1;
        this.op2 = op2;
        this.condition = condition;

        if (op1.getType().equals(op2.getType())) {
            this.type = op1.getType();
        } else {
            int line = ctx.getStart().getLine();
            String error = "Error: Ternary types for on line " + line + " don't match";
            System.out.println(error);
            System.exit(0);
        }
    }

    public String getType() {
        return type;
    }

    public Operation getOp1() {
        return op1;
    }

    public Operation getOp2() {
        return op2;
    }

    public Condition getCondition() {
        return condition;
    }
}
