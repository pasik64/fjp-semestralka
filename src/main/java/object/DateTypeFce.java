package object;

/**
 * RULE
 * dateTypeFce : dateType | VOID
 */
public class DateTypeFce extends ExpressionAbstract {
    private String type;

    public DateTypeFce(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }
}
