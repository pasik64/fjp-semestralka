package object;

/**
 * RULE
 * numeralOperationInner multiplyOperator numeralOperationFinal | numeralOperationFinal
 */
public class NumeralOperationInner extends ExpressionAbstract {
    private NumeralOperationInner numeralOperationInner;
    private MultiplyOperator multiplyOperator;
    private NumeralOperationFinal numeralOperationFinal;

    public NumeralOperationInner(NumeralOperationInner numeralOperationInner, MultiplyOperator multiplyOperator, NumeralOperationFinal numeralOperationFinal) {
        this.multiplyOperator = multiplyOperator;
        this.numeralOperationInner = numeralOperationInner;
        this.numeralOperationFinal = numeralOperationFinal;
    }

    public NumeralOperationInner(NumeralOperationFinal numeralOperationFinal) {
        this.numeralOperationFinal = numeralOperationFinal;
    }

    public MultiplyOperator getMultiplyOperator() {
        return multiplyOperator;
    }

    public NumeralOperationFinal getNumeralOperationFinal() {
        return numeralOperationFinal;
    }

    public NumeralOperationInner getNumeralOperationInner() {
        return numeralOperationInner;
    }
}
