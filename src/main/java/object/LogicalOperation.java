package object;

import java.util.ArrayList;
import java.util.List;

/**
 * RULE
 * logicalOperation : negateLogicalOperation (logicalOperatorBinary negateLogicalOperation)*
 */
public class LogicalOperation extends OperationAbstract {
    private List<NegateLogicalOperation> negateLogicalOperationList;
    private List<LogicalOperatorBinary> logicalOperatorBinaryList;

    public LogicalOperation() {
        negateLogicalOperationList = new ArrayList<>();
        logicalOperatorBinaryList = new ArrayList<>();
    }

    public LogicalOperation(List negateLogicalOperationList, List logicalOperatorBinaryList) {
        this.negateLogicalOperationList = negateLogicalOperationList;
        this.logicalOperatorBinaryList = logicalOperatorBinaryList;
    }

    public void addLogicalOperation(NegateLogicalOperation negateLogicalOperation) {
        negateLogicalOperationList.add(negateLogicalOperation);
    }

    public void addLogicalOperation(NegateLogicalOperation negateLogicalOperation, LogicalOperatorBinary logicalOperatorBinary) {
        negateLogicalOperationList.add(negateLogicalOperation);
        logicalOperatorBinaryList.add(logicalOperatorBinary);
    }

    public List<LogicalOperatorBinary> getLogicalOperationBinaryList() {
        return logicalOperatorBinaryList;
    }

    public List<NegateLogicalOperation> getNegateLogicalOperationList() {
        return negateLogicalOperationList;
    }
}
