package object;

import java.util.List;

/**
 * RULE
 * untilFunction : UNTIL_KEYWORD condition functionBody
 */
public class UntilFunction extends ExpressionAbstract {

    private Condition condition;
    private FunctionBody functionBody;
    private List<ReturnValue> returnValueList;

    public UntilFunction(Condition condition, FunctionBody functionBody) {
        this.condition = condition;
        this.functionBody = functionBody;
        this.returnValueList = functionBody.getReturnValueList();
    }

    public FunctionBody getFunctionBody() {
        return functionBody;
    }

    public Condition getCondition() {
        return condition;
    }

    public List<ReturnValue> getReturnValueList() {
        return returnValueList;
    }
}
