package object;

public class NumeralOperationFinal extends ExpressionAbstract {
    private BracketsOperation bracketsOperation;
    private Number number;
    private Identifier identifier;

    public NumeralOperationFinal(BracketsOperation bracketsOperation) {
        this.bracketsOperation = bracketsOperation;
    }

    public NumeralOperationFinal(Number number) {
        this.number = number;
    }

    public NumeralOperationFinal(Identifier identifier) {
        this.identifier = identifier;
    }

    public BracketsOperation getBracketsOperation() {
        return bracketsOperation;
    }

    public Number getNumber() {
        return number;
    }

    public Identifier getIdentifier() {
        return identifier;
    }
}
