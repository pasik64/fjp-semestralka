package object;

import java.util.List;

/**
 * RULE
 * callFunctionParameters : OPEN_BRACKET ((identifier | booleanValue | number) (COMMA (identifier | booleanValue | number))*)? CLOSE_BRACKET
 */
public class CallFunctionParameters extends ExpressionAbstract{
    private List<CallParameterAbstract> identifierList;

    public CallFunctionParameters(List identifierList) {
        this.identifierList = identifierList;
    }

    public List<CallParameterAbstract> getIdentifierList() {
        return identifierList;
    }
}
