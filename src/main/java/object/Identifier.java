package object;

/**
 * RULE
 * identifier : WORD
 */
public class Identifier extends CallParameterAbstract {

    private String identifier;

    public Identifier (String identifier) {
        this.identifier = identifier;
    }

    public String getIdentifier() {
        return identifier;
    }
}
