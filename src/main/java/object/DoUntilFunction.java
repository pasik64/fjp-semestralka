package object;

import java.util.List;

/**
 * RULE
 * doUntilFunction : DO_KEYWORD functionBody UNTIL_KEYWORD condition
 */
public class DoUntilFunction extends ExpressionAbstract {

    private Condition condition;
    private FunctionBody functionBody;
    private List<ReturnValue> returnValueList;

    public DoUntilFunction(Condition condition, FunctionBody functionBody) {
        this.condition = condition;
        this.functionBody = functionBody;
        this.returnValueList = functionBody.getReturnValueList();
    }

    public FunctionBody getFunctionBody() {
        return functionBody;
    }

    public Condition getCondition() {
        return condition;
    }

    public List<ReturnValue> getReturnValueList() {
        return returnValueList;
    }
}
