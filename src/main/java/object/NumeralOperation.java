package object;

/**
 * RULE
 * numeralOperation: numeralOperation addOperator numeralOperationInner | numeralOperationInner
 */
public class NumeralOperation extends OperationAbstract{
    private NumeralOperation numeralOperation;
    private AddOperator addOperator;
    private NumeralOperationInner numeralOperationInner;

    public NumeralOperation(NumeralOperationInner numeralOperationInner) {
        this.numeralOperationInner = numeralOperationInner;
    }

    public NumeralOperation(NumeralOperation numeralOperation, AddOperator addOperator, NumeralOperationInner numeralOperationInner) {
        this.numeralOperation = numeralOperation;
        this.addOperator = addOperator;
        this.numeralOperationInner = numeralOperationInner;
    }

    public NumeralOperationInner getNumeralOperationInner() {
        return numeralOperationInner;
    }

    public NumeralOperation getNumeralOperation() {
        return numeralOperation;
    }

    public AddOperator getAddOperator() {
        return addOperator;
    }
}
