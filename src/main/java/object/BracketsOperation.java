package object;

/**
 * RULE
 * bracketsOperation : (OPEN_BRACKET numeralOperation CLOSE_BRACKET)
 */
public class BracketsOperation extends ExpressionAbstract {
    private NumeralOperation numeralOperation;

    public BracketsOperation (NumeralOperation numeralOperation) {
        this.numeralOperation = numeralOperation;
    }

    public NumeralOperation getNumeralOperation() {
        return numeralOperation;
    }
}
