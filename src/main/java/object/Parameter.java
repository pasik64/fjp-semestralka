package object;

/**
 * RULE
 * parameter : dateType identifier
 */
public class Parameter extends ExpressionAbstract {

    private DateType dateType;
    private Identifier identifier;

    public Parameter(DateType dateType, Identifier identifier) {
        this.dateType = dateType;
        this.identifier = identifier;
    }

    public Identifier getIdentifier() {
        return identifier;
    }

    public DateType getDateType() {
        return dateType;
    }
}
