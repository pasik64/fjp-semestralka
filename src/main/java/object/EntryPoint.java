package object;

import java.util.ArrayList;
import java.util.List;

/**
 * RULE
 * entryPoint: (defineVariable | defineFunction | callFunction)*
 */
public class EntryPoint extends ExpressionAbstract{
    private List<ExpressionAbstract> expressionList;

    public EntryPoint() {
        expressionList = new ArrayList<>();
    }

    public EntryPoint(List expressionList) {
        this.expressionList = expressionList;
    }

    public void addExpression(ExpressionAbstract expression) {
        expressionList.add(expression);
    }

    public List<ExpressionAbstract> getExpressionList() {
        return expressionList;
    }
}
