package object;

/**
 * RULE
 * logicalOperatorBinary : AND | OR
 */
public class LogicalOperatorBinary extends ExpressionAbstract {

    private String type;

    public LogicalOperatorBinary(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }
}
