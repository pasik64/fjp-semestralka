package object;

import gen.GrammarParser;

import java.util.ArrayList;
import java.util.List;

/**
 * RULE
 * parameters : OPEN_BRACKET (parameter (COMMA parameter)*)? CLOSE_BRACKET
 */
public class Parameters extends ExpressionAbstract {

    private List<Parameter> parameterList;

    public Parameters() {
        parameterList = new ArrayList<>();
    }

    public void addParameter(Parameter parameter, GrammarParser.ParametersContext ctx) {
        if (this.checkIfExist(parameter.getIdentifier().getIdentifier())) {
            int line = ctx.getStart().getLine();
            String error = "Error: Function parameter " + parameter.getIdentifier().getIdentifier() + " on line " + line + " was already declared";
            System.out.println(error);
            System.exit(0);
        } else {
            parameterList.add(parameter);
        }
    }

    public List<Parameter> getParameterList() {
        return parameterList;
    }

    private boolean checkIfExist(String value) {
        for (Parameter parameter : parameterList) {
            String paramName = parameter.getIdentifier().getIdentifier();
            if (paramName.equals(value)) {
                return true;
            }
        }
        return false;
    }
}
