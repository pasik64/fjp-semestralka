package object;

/**
 * RULE
 * defineFunction : FUNCTION_KEYWORD dateTypeFce identifier parameters functionBody
 */
public class DefineFunction extends ExpressionAbstract {
    private DateTypeFce dateTypeFce;
    private Identifier identifier;
    private Parameters parameters;
    private FunctionBody functionBody;

    public DefineFunction(DateTypeFce dateTypeFce, Identifier identifier, Parameters parameters, FunctionBody functionBody) {
        this.dateTypeFce = dateTypeFce;
        this.identifier = identifier;
        this.parameters = parameters;
        this.functionBody = functionBody;
    }

    public FunctionBody getFunctionBody() {
        return functionBody;
    }

    public Identifier getIdentifier() {
        return identifier;
    }

    public DateTypeFce getDateTypeFce() {
        return dateTypeFce;
    }

    public Parameters getParameters() {
        return parameters;
    }
}
