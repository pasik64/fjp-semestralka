package object;

/**
 * RULE
 * number : (MINUS)?NUMBER(NUMBER)*
 */
public class Number extends CallParameterAbstract {
    private String value;

    public Number(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
