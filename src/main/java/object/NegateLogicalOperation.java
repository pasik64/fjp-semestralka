package object;


/**
 * RULE
 * negateLogicalOperation : (NEGATION)? (logicalOperationInner)
 */
public class NegateLogicalOperation extends ExpressionAbstract {
    private boolean negation;
    private LogicalOperationInner operation;

    public NegateLogicalOperation(boolean negation, LogicalOperationInner operation) {
        this.negation = negation;
        this.operation = operation;
    }

    public boolean isNegation() {
        return negation;
    }

    public LogicalOperationInner getOperation() {
        return operation;
    }
}
