package object;

import java.util.List;

/**
 * RULE
 * returnValue : RETURN_KEYWORD (identifier | number | booleanValue)? SEMICOLON
 */
public class ReturnValue extends ExpressionAbstract {

    private Identifier identifier;
    private BooleanValue booleanValue;
    private Number number;

    public ReturnValue() {
    }

    public ReturnValue(Identifier identifier) {
        this.identifier = identifier;
    }

    public ReturnValue(BooleanValue booleanValue) {
        this.booleanValue = booleanValue;
    }

    public ReturnValue(Number number) {
        this.number = number;
    }

    public BooleanValue getBooleanValue() {
        return booleanValue;
    }

    public Identifier getIdentifier() {
        return identifier;
    }

    public Number getNumber() {
        return number;
    }

    public String getType(List<DefineVariable> vars) {
        if (booleanValue != null) {
            return "boolean";
        } else if (number != null) {
            return "int";
        } else {
            DefineVariable var = this.findVariable(identifier, vars);
            if (var.getDateType().getType().equals("int")) {
                return "int";
            } else return var.getDateType().getType();
        }
    }

    private DefineVariable findVariable(Identifier identifier, List<DefineVariable> vars) {
        for (DefineVariable item : vars) {
            String type = item.getVariableType();
            if (type.equals("normal")) {
                if (item.getIdentifier().getIdentifier().equals(identifier.getIdentifier())) {
                    return item;
                }
            } else {
                for (Identifier subitem : item.getIdentifierList()) {
                    if (subitem.getIdentifier().equals(identifier.getIdentifier())) {
                        return item;
                    }
                }
            }
        }
        return null;
    }

    public boolean isEmpty() {
        return identifier == null && number == null && booleanValue == null;
    }
}
