package object;

import java.util.List;

/**
 * RULE
 * ifFunction : IF_KEYWORD condition functionBody (ElSE_KEYWORD functionBody)?
 */
public class IfFunction extends ExpressionAbstract {
    private Condition condition;
    private FunctionBody functionBody;
    private FunctionBody elseBody;
    private List<ReturnValue> returnValueList;

    public IfFunction(Condition condition, FunctionBody functionBody) {
        this.condition = condition;
        this.functionBody = functionBody;
        this.returnValueList = functionBody.getReturnValueList();
    }

    public IfFunction(Condition condition, FunctionBody functionBody, FunctionBody elseBody) {
        this.condition = condition;
        this.functionBody = functionBody;
        this.elseBody = elseBody;
        this.returnValueList = functionBody.getReturnValueList();
        this.returnValueList.addAll(elseBody.getReturnValueList());
    }

    public Condition getCondition() {
        return condition;
    }

    public FunctionBody getFunctionBody() {
        return functionBody;
    }

    public List<ReturnValue> getReturnValueList() {
        return returnValueList;
    }

    public FunctionBody getElseBody() {
        return elseBody;
    }
}
