package object;

public class AddOperator extends ExpressionAbstract{
    public String type;

    public AddOperator(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }
}
