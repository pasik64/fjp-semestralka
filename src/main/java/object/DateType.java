package object;

/**
 * RULE
 * dateType : INT | STRING | BOOLEAN
 */
public class DateType extends ExpressionAbstract {

    private String type;

    public DateType(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }
}
