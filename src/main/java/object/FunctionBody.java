package object;

import java.util.ArrayList;
import java.util.List;

/**
 * RULE
 * functionBody : OPEN_FUNC_BRACKET (functionRow)* CLOSE_FUNC_BRACKET
 */
public class FunctionBody extends ExpressionAbstract{
    private List<FunctionRow> functionRowList;
    private List<ReturnValue> returnValueList;

    public FunctionBody() {
        functionRowList = new ArrayList<>();
        returnValueList = new ArrayList<>();
    }

    public FunctionBody(List functionRowList) {
        this.functionRowList = functionRowList;
        returnValueList = new ArrayList<>();
    }

    public FunctionBody(List functionRowList, List returnValueList) {
        this.functionRowList = functionRowList;
        this.returnValueList = returnValueList;
    }

    public void addFunctionRow(FunctionRow functionRow) {
        functionRowList.add(functionRow);
        List<ReturnValue> value = functionRow.getReturnValueList();
        if (!value.isEmpty()) {
            returnValueList.addAll(value);
        }
    }

    public List<FunctionRow> getFunctionRowList() {
        return functionRowList;
    }

    public List<ReturnValue> getReturnValueList() {
        return returnValueList;
    }
}
