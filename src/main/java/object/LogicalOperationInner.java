package object;

/**
 * RULE
 * logicalOperationInner: identifier | booleanValue | numeralOperation logicalOperator numeralOperation
 */
public class LogicalOperationInner extends ExpressionAbstract {
    private Identifier identifier;
    private BooleanValue booleanValue;

    private NumeralOperation numeralOperation1;
    private LogicalOperator logicalOperator;
    private NumeralOperation numeralOperation2;

    public LogicalOperationInner(Identifier identifier) {
        this.identifier = identifier;
    }

    public LogicalOperationInner(BooleanValue booleanValue) {
        this.booleanValue = booleanValue;
    }

    public LogicalOperationInner(NumeralOperation numeralOperation1, LogicalOperator logicalOperator, NumeralOperation numeralOperation2) {
        this.numeralOperation1 = numeralOperation1;
        this.logicalOperator = logicalOperator;
        this.numeralOperation2 = numeralOperation2;
    }

    public BooleanValue getBooleanValue() {
        return booleanValue;
    }

    public Identifier getIdentifier() {
        return identifier;
    }

    public LogicalOperator getLogicalOperator() {
        return logicalOperator;
    }

    public NumeralOperation getNumeralOperator1() {
        return numeralOperation1;
    }

    public NumeralOperation getNumeralOperator2() {
        return numeralOperation2;
    }

}
