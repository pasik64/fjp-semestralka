package object;

/**
 * RULE
 * booleanValue : TRUE | FALSE
 */
public class BooleanValue extends CallParameterAbstract {
    private String value;

    public BooleanValue(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
