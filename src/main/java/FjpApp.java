import gen.GrammarLexer;
import gen.GrammarParser;
import object.EntryPoint;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ErrorNodeImpl;
import org.antlr.v4.runtime.tree.ParseTree;
import visitor.ProgramVisitor;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class FjpApp {

    public static void main(String[] args) {

        if (args.length != 2) {
            System.out.println("Program called with invalid arguments");
            System.out.println("Input and output files must be provided");
            System.exit(-1);
        } else {
            System.out.println("FJP language converter started");
        }

        CharStream input = null;
        try {
            input = CharStreams.fromFileName(args[0]);
        } catch (Exception e) {
            System.out.println("Input file wasn't found");
            System.exit(-1);
        }


        GrammarLexer lexer = new GrammarLexer(input);
        CommonTokenStream token_stream = new CommonTokenStream(lexer);
        GrammarParser parser = new GrammarParser(token_stream);

        ParseTree tree = parser.entryPoint();
        checkErrors(tree);
        ProgramVisitor programVisitor = new ProgramVisitor();
        EntryPoint program = programVisitor.visit(tree);

        try {
            FileWriter writer = new FileWriter(new File(args[1]));
            Compiler compiler = new Compiler(writer, program);
            compiler.generatePL0();
            writer.flush();
            writer.close();
        } catch (IOException e) {
            System.out.println("Output file couldn't be created");
            System.exit(-1);
        }

    }

    public static void checkErrors(ParseTree parseTree) {
        for (int i = 0; i < parseTree.getChildCount(); i++) {
            if (parseTree.getChild(i) instanceof ErrorNodeImpl) {
                System.exit(-1);
            } else {
                checkErrors(parseTree.getChild(i));
            }
        }
    }

}
