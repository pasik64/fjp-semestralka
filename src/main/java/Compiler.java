import object.*;
import object.Number;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

public class Compiler {
    private FileWriter writer;
    private EntryPoint program;
    private int lineNumber;
    private ArrayList<String> commands;
    private HashMap<String, Integer> functionMap;
    private ArrayList<Identifier> identifiers;
    private ArrayList<DefineFunction> defineFunctions;
    private int level;

    public Compiler(FileWriter writer, EntryPoint program) {
        level = 0;
        defineFunctions = new ArrayList<>();
        identifiers = new ArrayList<>();
        functionMap = new HashMap<>();
        this.writer = writer;
        this.program = program;
    }

    private void saveCode(String s) {
        commands.add(lineNumber + " " + s + "\n");
        lineNumber++;
    }

    private void updateCode(String s, int index) {
        commands.set(index, index + " " + s + "\n");
    }

    public void generatePL0() throws IOException {
        commands = new ArrayList<>();
        lineNumber = 0;
        loadVariables(program);
        saveCode("JMP 0 X");
        for (ExpressionAbstract expressionAbstract: program.getExpressionList()) {
            if (expressionAbstract instanceof DefineFunction) {
                generateDefineFunctionPL0((DefineFunction)expressionAbstract);
            }
        }
        updateCode("JMP 0 " + commands.size(), 0);
        saveCode("INT 0 " + (identifiers.size() + defineFunctions.size() + 3));
        for (ExpressionAbstract expressionAbstract: program.getExpressionList()) {
            if (expressionAbstract instanceof DefineVariable) {
                generateDefineVariablePL0((DefineVariable) expressionAbstract);
            } else if (expressionAbstract instanceof IfFunction) {
                generateIFFunctionPL0((IfFunction) expressionAbstract, null);
            } else if (expressionAbstract instanceof WhileFunction) {
                generateWhileFunctionPL0((WhileFunction) expressionAbstract, null);
            } else if (expressionAbstract instanceof  DoWhileFunction) {
                generateDoWhileFunctionPL0((DoWhileFunction) expressionAbstract, null);
            } else if (expressionAbstract instanceof  DoUntilFunction) {
                generateDoUntilFunctionPL0((DoUntilFunction) expressionAbstract, null);
            } else if (expressionAbstract instanceof UntilFunction) {
                generateUntilFunctionPL0((UntilFunction) expressionAbstract, null);
            } else if (expressionAbstract instanceof ForFunction) {
                generateForFunctionPL0((ForFunction) expressionAbstract, null);
            } else if (expressionAbstract instanceof CallFunction) {
                generateCallFunctionPL0((CallFunction) expressionAbstract);
            }
        }
        saveCode("RET 0 0");
        writeCommands();
    }

    private void generateDefineFunctionPL0(DefineFunction defineFunction) {
        defineFunctions.add(defineFunction);
        functionMap.put(defineFunction.getIdentifier().getIdentifier(), commands.size());
        saveCode("INT 0 3");
        level++;
        generateFunctionRowListPL0(defineFunction.getFunctionBody().getFunctionRowList(), defineFunction);
        if (!commands.get(commands.size()-1).contains("RET 0 0")) {
            saveCode("RET 0 0");
        }
        level--;
    }

    private void generateWhileFunctionPL0(WhileFunction whileFunction, DefineFunction defineFunction) {
        int condIndex = commands.size();
        generateLogicalOperationPL0(whileFunction.getCondition().getLogicalOperation());
        int index = commands.size();
        saveCode("JMC 0 X");
        generateFunctionRowListPL0(whileFunction.getFunctionBody().getFunctionRowList(), defineFunction);
        saveCode("JMP 0 " + condIndex);
        updateCode("JMC 0 " + commands.size(), index);
    }

    private void writeCommands() throws IOException {
        for (String command: commands) {
            writer.write(command);
        }
    }

    private void generateIFFunctionPL0(IfFunction ifFunction, DefineFunction defineFunction) {
        generateLogicalOperationPL0(ifFunction.getCondition().getLogicalOperation());
        int index = commands.size();
        saveCode("JMC 0 X");
        generateFunctionRowListPL0(ifFunction.getFunctionBody().getFunctionRowList(), defineFunction);
        updateCode("JMC 0 " + commands.size(), index);
        if (ifFunction.getElseBody() != null) {
            int elseStartIndex = commands.size();
            saveCode("JMP 0 X");
            updateCode("JMC 0 " + commands.size(), index);
            generateFunctionRowListPL0(ifFunction.getElseBody().getFunctionRowList(), defineFunction);
            updateCode("JMP 0 " + commands.size(), elseStartIndex);
        } else {
            updateCode("JMC 0 " + commands.size(), index);
        }
    }

    private ArrayList<Integer> generateLogicalOperationPL0(LogicalOperation logicalOperation) {
        ArrayList<Integer> returnJumpIndexes = new ArrayList<>();
        ArrayList<Integer> localJumpIndexes = new ArrayList<>();
        for (int i = 0; i < logicalOperation.getNegateLogicalOperationList().size(); i++) {
            if ((i > 1) && logicalOperation.getLogicalOperationBinaryList().get(i-1).getType().equals("&&")) {
                for (int index : localJumpIndexes) {
                    updateCode("JMC 0 " + commands.size(), index);
                }
                localJumpIndexes.clear();
            }
            generateNegateOperationInnerPL0(logicalOperation.getNegateLogicalOperationList().get(i));
            if (logicalOperation.getLogicalOperationBinaryList().size() > i) {
                if (logicalOperation.getLogicalOperationBinaryList().get(i).getType().equals("&&")) {
                    returnJumpIndexes.add(lineNumber);
                    saveCode("JMC 0 X");
                } else if (logicalOperation.getLogicalOperationBinaryList().get(i).getType().equals("||")) {
                    saveCode("LIT 0 0");
                    saveCode("OPR 0 8");
                    localJumpIndexes.add(lineNumber);
                    saveCode("JMC 0 Z");
                }
            }
            if (i > 0) {
                if (logicalOperation.getLogicalOperationBinaryList().get(i-1).getType().equals("&&")) {
                    if (!commands.get(commands.size()-1).equals((commands.size() - 1) + " JMC 0 X\n")) {
                        returnJumpIndexes.add(lineNumber);
                        saveCode("JMC 0 X");
                    }
                }
            }
        }
        if (logicalOperation.getLogicalOperationBinaryList().size() > 1) {
            saveCode("LIT 0 1");
            saveCode("JMP 0 " + (commands.size() + 2));
            for (int index : returnJumpIndexes) {
                updateCode("JMC 0 " + commands.size(), index);
            }
            saveCode("LIT 0 0");
            saveCode("LIT 0 1");
            saveCode("OPR 0 8");
        }

        return returnJumpIndexes;
    }

    private void generateNegateOperationInnerPL0(NegateLogicalOperation negateLogicalOperation) {
        generateLogicalOperationInnerPL0(negateLogicalOperation.getOperation());
        if (negateLogicalOperation.isNegation()) {
            saveCode("LIT 0 0");
            saveCode("OPR 0 8");
        }
    }

    private void generateLogicalOperationInnerPL0(LogicalOperationInner operation) {
        if (operation.getNumeralOperator1() != null) {
            generateNumeralOperationPL0(operation.getNumeralOperator1());
            generateNumeralOperationPL0(operation.getNumeralOperator2());
            generateLogicalOperatorPL0(operation.getLogicalOperator());
        } else if (operation.getBooleanValue() != null) {
            if (operation.getBooleanValue().getValue().equals("true")) {
                saveCode("LIT 0 1");
            } else if (operation.getBooleanValue().getValue().equals("false")) {
                saveCode("LIT 0 0");
            }
        } else if (operation.getIdentifier() != null) {
            int index = getVariableIndex(operation.getIdentifier());
            if (index != -1) {
                saveCode("LOD " + level + "  " + (index + 3));
            }
        }
    }

    private void generateLogicalOperatorPL0(LogicalOperator logicalOperator) {
        switch (logicalOperator.getType()) {
            case "==" :
                saveCode("OPR 0 8");
                break;
            case "!=" :
                saveCode("OPR 0 9");
                break;
            case "<" :
                saveCode("OPR 0 10");
                break;
            case ">=" :
                saveCode("OPR 0 11");
                break;
            case ">" :
                saveCode("OPR 0 12");
                break;
            case "<=" :
                saveCode("OPR 0 13");
                break;
        }
    }

    private void generateFunctionRowListPL0(List<FunctionRow> functionRowList, DefineFunction defineFunction) {
        for (FunctionRow row : functionRowList) {
            if (row.getDefineVariable() != null) {
                generateDefineVariablePL0(row.getDefineVariable());
            } else if (row.getIfFunction() != null) {
                generateIFFunctionPL0(row.getIfFunction(), defineFunction);
            } else if (row.getWhileFunction() != null) {
                generateWhileFunctionPL0(row.getWhileFunction(), defineFunction);
            } else if (row.getFunctionCall() != null) {
                generateCallFunctionPL0(row.getFunctionCall());
            } else if (row.getDoWhileFunction() != null) {
                generateDoWhileFunctionPL0(row.getDoWhileFunction(), defineFunction);
            } else if (row.getDoUntilFunction() != null) {
                generateDoUntilFunctionPL0(row.getDoUntilFunction(), defineFunction);
            } else if (row.getUntilFunction() != null) {
                generateUntilFunctionPL0(row.getUntilFunction(), defineFunction);
            } else if (row.getForFunction() != null) {
                generateForFunctionPL0(row.getForFunction(), defineFunction);
            } else if (row.getReturnValueList() != null && row.getReturnValueList().size() == 1) {
                generateReturnValuePL0(row.getReturnValueList().get(0), defineFunction);
            } else if (row.isReturn()) {
                saveCode("RET 0 0");
            }
        }
    }

    private void generateForFunctionPL0(ForFunction forFunction, DefineFunction defineFunction) {
        generateNumeralOperationPL0(forFunction.getInitOperation());
        saveCode("STO " + level + " " + (getVariableIndex(forFunction.getInitIdentifier()) + 3));
        int condIndex = commands.size();
        generateLogicalOperationPL0(forFunction.getCondition());
        int index = commands.size();
        saveCode("JMC 0 X");
        generateFunctionRowListPL0(forFunction.getFunctionBody().getFunctionRowList(), defineFunction);
        generateNumeralOperationPL0(forFunction.getOperation());
        saveCode("STO " + level + " " + (getVariableIndex(forFunction.getIdentifier()) + 3));
        saveCode("JMP 0 " + condIndex);
        updateCode("JMC 0 " + commands.size(), index);
    }

    private void generateUntilFunctionPL0(UntilFunction untilFunction, DefineFunction defineFunction) {
        int condIndex = commands.size();
        generateLogicalOperationPL0(untilFunction.getCondition().getLogicalOperation());
        saveCode("LIT 0 0");
        saveCode("OPR 0 8");
        int index = commands.size();
        saveCode("JMC 0 X");
        generateFunctionRowListPL0(untilFunction.getFunctionBody().getFunctionRowList(), defineFunction);
        saveCode("JMP 0 " + condIndex);
        updateCode("JMC 0 " + commands.size(), index);
    }

    private void generateDoUntilFunctionPL0(DoUntilFunction doUntilFunction, DefineFunction defineFunction) {
        int startIndex = commands.size();
        generateFunctionRowListPL0(doUntilFunction.getFunctionBody().getFunctionRowList(), defineFunction);
        generateLogicalOperationPL0(doUntilFunction.getCondition().getLogicalOperation());
        saveCode("LIT 0 0");
        saveCode("OPR 0 8");
        saveCode("JMC 0 " + (commands.size() + 2));
        saveCode("JMP 0 " + startIndex);
    }

    private void generateDoWhileFunctionPL0(DoWhileFunction doWhileFunction, DefineFunction defineFunction) {
        int startIndex = commands.size();
        generateFunctionRowListPL0(doWhileFunction.getFunctionBody().getFunctionRowList(), defineFunction);
        generateLogicalOperationPL0(doWhileFunction.getCondition().getLogicalOperation());
        saveCode("JMC 0 " + (commands.size() + 2));
        saveCode("JMP 0 " + startIndex);
    }

    private void generateReturnValuePL0(ReturnValue returnValue, DefineFunction defineFunction) {
        int index = getFunctionIndex(defineFunction.getIdentifier());
        if (returnValue.getIdentifier() != null) {
            saveCode("LOD " + level + " " + (getVariableIndex(returnValue.getIdentifier()) + 3));
        } else if (returnValue.getBooleanValue() != null) {
            if (returnValue.getBooleanValue().getValue().equals("true")) {
                saveCode("LIT 0 1");
            } else {
                saveCode("LIT 0 0");
            }
        } else if (returnValue.getNumber() != null) {
            saveCode("LIT 0 " + returnValue.getNumber().getValue());
        }
        saveCode("STO " + level + " " + (index + identifiers.size() + 3));
        saveCode("RET 0 0");
    }

    private int getFunctionIndex(Identifier identifier) {
        int index = -1;
        for (int i = 0; i < defineFunctions.size(); i++) {
            if (defineFunctions.get(i).getIdentifier().getIdentifier().equals(identifier.getIdentifier())) {
                index = i;
            }
        }
        return index;
    }

    private void generateCallFunctionPL0(CallFunction callFunction) {
        DefineFunction defineFunction = null;
        for (DefineFunction function : defineFunctions) {
            if (function.getIdentifier().getIdentifier().equals(callFunction.getIdentifier().getIdentifier())) {
                defineFunction = function;
            }
        }
        for (int i = 0; i < Objects.requireNonNull(defineFunction).getParameters().getParameterList().size(); i++) {
            if (callFunction.getCallFunctionParameters().getIdentifierList().get(i) instanceof Identifier) {
                saveCode("LOD " + level + " " + (getVariableIndex((Identifier)callFunction.getCallFunctionParameters().getIdentifierList().get(i)) + 3));
            } else if (callFunction.getCallFunctionParameters().getIdentifierList().get(i) instanceof Number) {
                saveCode("LIT 0 " + ((Number) callFunction.getCallFunctionParameters().getIdentifierList().get(i)).getValue());
            } else if (callFunction.getCallFunctionParameters().getIdentifierList().get(i) instanceof BooleanValue) {
                if (((BooleanValue) callFunction.getCallFunctionParameters().getIdentifierList().get(i)).getValue().equals("true")) {
                    saveCode("LIT 0 1");
                } else {
                    saveCode("LIT 0 0");
                }
            }
            saveCode("STO " + level + " " + (getVariableIndex(defineFunction.getParameters().getParameterList().get(i).getIdentifier())+3));
        }
        saveCode("CAL " + level + " " + functionMap.get(callFunction.getIdentifier().getIdentifier()));
        int functionIndex = getFunctionIndex(callFunction.getIdentifier());
        saveCode("LOD " + level + "  " + (functionIndex + identifiers.size() + 3));
    }

    private void tryAddDefineVariable(DefineVariable variable) {
        if (variable.getIdentifier() != null) {
            if (identifiers.stream().noneMatch(v -> v.getIdentifier().equals(variable.getIdentifier().getIdentifier()))) {
                identifiers.add(variable.getIdentifier());
            }
        } else if (variable.getIdentifierList() != null) {
            for (Identifier identifier : variable.getIdentifierList()) {
                if (identifiers.stream().noneMatch(v -> v.getIdentifier().equals(identifier.getIdentifier()))) {
                    identifiers.add(identifier);
                }
            }
        }
    }

    private void loadVariables(EntryPoint program) {
        for (ExpressionAbstract expressionAbstract: program.getExpressionList()) {
            if (expressionAbstract instanceof DefineVariable) {
                tryAddDefineVariable((DefineVariable)expressionAbstract);
            } else if (expressionAbstract instanceof WhileFunction) {
                loadVariablesInWhileFunction((WhileFunction) expressionAbstract);
            } else if (expressionAbstract instanceof IfFunction) {
                loadVariablesInIfFunction((IfFunction) expressionAbstract);
            } else if (expressionAbstract instanceof DefineFunction) {
                loadVariablesInDefineFunction((DefineFunction) expressionAbstract);
            } else if (expressionAbstract instanceof DoWhileFunction) {
                loadVariablesInDoWhileFunction((DoWhileFunction) expressionAbstract);
            } else if (expressionAbstract instanceof  DoUntilFunction) {
                loadVariablesInDoUntilFunction((DoUntilFunction) expressionAbstract);
            } else if (expressionAbstract instanceof  UntilFunction) {
                loadVariablesInUntilFunction((UntilFunction) expressionAbstract);
            } else if (expressionAbstract instanceof ForFunction) {
                loadVariablesInForFunction((ForFunction) expressionAbstract);
            }
        }
    }

    private void loadVariablesInForFunction(ForFunction forFunction) {
        if (identifiers.stream().noneMatch(v -> v.getIdentifier().equals(forFunction.getIdentifier().getIdentifier()))) {
            identifiers.add(forFunction.getIdentifier());
        }
        if (identifiers.stream().noneMatch(v -> v.getIdentifier().equals(forFunction.getInitIdentifier().getIdentifier()))) {
            identifiers.add(forFunction.getInitIdentifier());
        }
        loadVariablesInFunctionRowList(forFunction.getFunctionBody().getFunctionRowList());
    }

    private void loadVariablesInUntilFunction(UntilFunction untilFunction) {
        loadVariablesInFunctionRowList(untilFunction.getFunctionBody().getFunctionRowList());
    }

    private void loadVariablesInDoUntilFunction(DoUntilFunction doUntilFunction) {
        loadVariablesInFunctionRowList(doUntilFunction.getFunctionBody().getFunctionRowList());
    }

    private void loadVariablesInDoWhileFunction(DoWhileFunction doWhileFunction) {
        loadVariablesInFunctionRowList(doWhileFunction.getFunctionBody().getFunctionRowList());
    }

    private void loadVariablesInDefineFunction(DefineFunction defineFunction) {
        for (Parameter parameter : defineFunction.getParameters().getParameterList()) {
            if (identifiers.stream().noneMatch(v -> v.getIdentifier().equals(parameter.getIdentifier().getIdentifier()))) {
                identifiers.add(parameter.getIdentifier());
            }
        }

        loadVariablesInFunctionRowList(defineFunction.getFunctionBody().getFunctionRowList());
    }

    private void loadVariablesInWhileFunction(WhileFunction whileFunction) {
        loadVariablesInFunctionRowList(whileFunction.getFunctionBody().getFunctionRowList());
    }

    private void loadVariablesInFunctionRowList(List<FunctionRow> functionRowList) {
        for (FunctionRow row : functionRowList) {
            if (row.getDefineVariable() != null) {
                tryAddDefineVariable(row.getDefineVariable());
            } else if (row.getWhileFunction() != null) {
                loadVariablesInWhileFunction(row.getWhileFunction());
            } else if (row.getIfFunction() != null) {
                loadVariablesInIfFunction(row.getIfFunction());
            } else if (row.getForFunction() != null) {
                loadVariablesInForFunction(row.getForFunction());
            } else if (row.getUntilFunction() != null) {
                loadVariablesInUntilFunction(row.getUntilFunction());
            } else if (row.getDoUntilFunction() != null) {
                loadVariablesInDoUntilFunction(row.getDoUntilFunction());
            } else if (row.getDoWhileFunction() != null) {
                loadVariablesInDoWhileFunction(row.getDoWhileFunction());
            }
        }
    }

    private void loadVariablesInIfFunction(IfFunction ifFunction) {
        loadVariablesInFunctionRowList(ifFunction.getFunctionBody().getFunctionRowList());
    }

    private void generateDefineVariablePL0(DefineVariable expression) {
        if (expression.getIdentifier() != null) {
            OperationAbstract operationAbstract = expression.getOperation().getOperationAbstract();
            generateOperationAbstractPL0(operationAbstract);
            int index = getVariableIndex(expression.getIdentifier());
            saveCode("STO " + level + "  " + (index + 3));
        } else if (expression.getOperationList() != null) {
            for (int i = 0; i < expression.getOperationList().size(); i++) {
                if (expression.getOperationList().get(i).getOperationAbstract() instanceof NumeralOperation) {
                    generateNumeralOperationPL0((NumeralOperation) expression.getOperationList().get(i).getOperationAbstract());
                } else if (expression.getOperationList().get(i).getOperationAbstract() instanceof LogicalOperation) {
                    generateLogicalOperationPL0((LogicalOperation) expression.getOperationList().get(i).getOperationAbstract());
                }
                int index = getVariableIndex(expression.getIdentifierList().get(i));
                saveCode("STO " + level + "  " + (index + 3));
            }
        } else if (expression.getIdentifierList() != null) {
            OperationAbstract operationAbstract = expression.getOperation().getOperationAbstract();
            for (Identifier id : expression.getIdentifierList()) {
                if (operationAbstract instanceof NumeralOperation) {
                    generateNumeralOperationPL0((NumeralOperation) operationAbstract);
                } else if (operationAbstract instanceof LogicalOperation) {
                    generateLogicalOperationPL0((LogicalOperation) operationAbstract);
                }
                int index = getVariableIndex(id);
                saveCode("STO " + level + "  " + (index + 3));
            }

        }
    }

    private void generateTernaryOperationPL0(TernaryOperation ternaryOperation) {
        generateLogicalOperationPL0(ternaryOperation.getCondition().getLogicalOperation());
        int index = commands.size();
        saveCode("JMC 0 X");
        generateOperationAbstractPL0(ternaryOperation.getOp2().getOperationAbstract());
        int index2 = commands.size();
        saveCode("JMP 0 X");
        updateCode("JMC 0 " + commands.size(), index);

        generateOperationAbstractPL0(ternaryOperation.getOp1().getOperationAbstract());
        updateCode("JMP 0 " + commands.size(), index2);
    }

    private void generateOperationAbstractPL0(OperationAbstract operationAbstract) {
        if (operationAbstract instanceof NumeralOperation) {
            generateNumeralOperationPL0((NumeralOperation) operationAbstract);
        } else if (operationAbstract instanceof LogicalOperation) {
            generateLogicalOperationPL0((LogicalOperation) operationAbstract);
        } else if (operationAbstract instanceof CallFunction) {
            generateCallFunctionPL0((CallFunction) operationAbstract);
        } else if (operationAbstract instanceof TernaryOperation) {
            generateTernaryOperationPL0((TernaryOperation) operationAbstract);
        }
    }

    private int getVariableIndex(Identifier identifier) {
        int index = -1;
        for (Identifier id : identifiers) {
            if (id.getIdentifier().equals(identifier.getIdentifier())) {
                index = identifiers.indexOf(id);
                break;
            }
        }

        return index;
    }

    private void generateNumeralOperationPL0(NumeralOperation numeralOperation) {
        if (numeralOperation.getNumeralOperation() != null) {
            generateNumeralOperationPL0(numeralOperation.getNumeralOperation());
        }
        generateNumeralOperationInnerPL0(numeralOperation.getNumeralOperationInner());

        if (numeralOperation.getAddOperator() != null) {
            if (numeralOperation.getAddOperator().getType().equals("+")) {
                saveCode("OPR 0 2");
            } else if (numeralOperation.getAddOperator().getType().equals("-")) {
                saveCode("OPR 0 3");
            }
        }
    }

    private void generateNumeralOperationInnerPL0(NumeralOperationInner numeralOperationInner) {
        if (numeralOperationInner.getNumeralOperationInner() != null) {
            generateNumeralOperationInnerPL0(numeralOperationInner.getNumeralOperationInner());
        }
        generateNumeralOperationFinalPL0(numeralOperationInner.getNumeralOperationFinal());
        if (numeralOperationInner.getMultiplyOperator() != null) {
            if (numeralOperationInner.getMultiplyOperator().getType().equals("*")) {
                saveCode("OPR 0 4");
            } else if (numeralOperationInner.getMultiplyOperator().getType().equals("/")) {
                saveCode("OPR 0 5");
            }
        }
    }

    private void generateNumeralOperationFinalPL0(NumeralOperationFinal numeralOperationFinal) {
        if (numeralOperationFinal.getNumber() != null) {
            saveCode("LIT 0 " + Integer.valueOf(numeralOperationFinal.getNumber().getValue()));
        } else if (numeralOperationFinal.getIdentifier() != null) {
            int index = getVariableIndex(numeralOperationFinal.getIdentifier());
            if (index != -1) {
                saveCode("LOD " + level + "  " + (index + 3));
            }
        } else if (numeralOperationFinal.getBracketsOperation() != null) {
            generateNumeralOperationPL0(numeralOperationFinal.getBracketsOperation().getNumeralOperation());
        }
    }
}
