// Generated from C:/Users/HP/IdeaProjects/fjp-semestralka/src/main\Grammar.g4 by ANTLR 4.8
package gen;
import org.antlr.v4.runtime.tree.ParseTreeVisitor;

/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by {@link GrammarParser}.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 * operations with no return type.
 */
public interface GrammarVisitor<T> extends ParseTreeVisitor<T> {
	/**
	 * Visit a parse tree produced by {@link GrammarParser#entryPoint}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitEntryPoint(GrammarParser.EntryPointContext ctx);
	/**
	 * Visit a parse tree produced by {@link GrammarParser#booleanValue}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBooleanValue(GrammarParser.BooleanValueContext ctx);
	/**
	 * Visit a parse tree produced by {@link GrammarParser#number}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNumber(GrammarParser.NumberContext ctx);
	/**
	 * Visit a parse tree produced by {@link GrammarParser#identifier}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIdentifier(GrammarParser.IdentifierContext ctx);
	/**
	 * Visit a parse tree produced by {@link GrammarParser#dateType}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDateType(GrammarParser.DateTypeContext ctx);
	/**
	 * Visit a parse tree produced by {@link GrammarParser#dateTypeFce}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDateTypeFce(GrammarParser.DateTypeFceContext ctx);
	/**
	 * Visit a parse tree produced by {@link GrammarParser#create}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCreate(GrammarParser.CreateContext ctx);
	/**
	 * Visit a parse tree produced by {@link GrammarParser#addOperator}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAddOperator(GrammarParser.AddOperatorContext ctx);
	/**
	 * Visit a parse tree produced by {@link GrammarParser#multiplyOperator}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMultiplyOperator(GrammarParser.MultiplyOperatorContext ctx);
	/**
	 * Visit a parse tree produced by {@link GrammarParser#logicalOperator}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLogicalOperator(GrammarParser.LogicalOperatorContext ctx);
	/**
	 * Visit a parse tree produced by {@link GrammarParser#logicalOperatorBinary}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLogicalOperatorBinary(GrammarParser.LogicalOperatorBinaryContext ctx);
	/**
	 * Visit a parse tree produced by {@link GrammarParser#logicalOperation}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLogicalOperation(GrammarParser.LogicalOperationContext ctx);
	/**
	 * Visit a parse tree produced by {@link GrammarParser#negateLogicalOperation}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNegateLogicalOperation(GrammarParser.NegateLogicalOperationContext ctx);
	/**
	 * Visit a parse tree produced by {@link GrammarParser#logicalOperationInner}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLogicalOperationInner(GrammarParser.LogicalOperationInnerContext ctx);
	/**
	 * Visit a parse tree produced by {@link GrammarParser#numeralOperation}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNumeralOperation(GrammarParser.NumeralOperationContext ctx);
	/**
	 * Visit a parse tree produced by {@link GrammarParser#numeralOperationInner}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNumeralOperationInner(GrammarParser.NumeralOperationInnerContext ctx);
	/**
	 * Visit a parse tree produced by {@link GrammarParser#numeralOperationFinal}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNumeralOperationFinal(GrammarParser.NumeralOperationFinalContext ctx);
	/**
	 * Visit a parse tree produced by {@link GrammarParser#bracketsOperation}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBracketsOperation(GrammarParser.BracketsOperationContext ctx);
	/**
	 * Visit a parse tree produced by {@link GrammarParser#operation}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOperation(GrammarParser.OperationContext ctx);
	/**
	 * Visit a parse tree produced by {@link GrammarParser#ternaryOperation}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTernaryOperation(GrammarParser.TernaryOperationContext ctx);
	/**
	 * Visit a parse tree produced by {@link GrammarParser#parameters}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitParameters(GrammarParser.ParametersContext ctx);
	/**
	 * Visit a parse tree produced by {@link GrammarParser#parameter}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitParameter(GrammarParser.ParameterContext ctx);
	/**
	 * Visit a parse tree produced by {@link GrammarParser#condition}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCondition(GrammarParser.ConditionContext ctx);
	/**
	 * Visit a parse tree produced by {@link GrammarParser#defineVariable}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDefineVariable(GrammarParser.DefineVariableContext ctx);
	/**
	 * Visit a parse tree produced by {@link GrammarParser#multipleIdentifiers}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMultipleIdentifiers(GrammarParser.MultipleIdentifiersContext ctx);
	/**
	 * Visit a parse tree produced by {@link GrammarParser#multipleOperations}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMultipleOperations(GrammarParser.MultipleOperationsContext ctx);
	/**
	 * Visit a parse tree produced by {@link GrammarParser#defineFunction}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDefineFunction(GrammarParser.DefineFunctionContext ctx);
	/**
	 * Visit a parse tree produced by {@link GrammarParser#callFunction}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCallFunction(GrammarParser.CallFunctionContext ctx);
	/**
	 * Visit a parse tree produced by {@link GrammarParser#callFunctionParameters}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCallFunctionParameters(GrammarParser.CallFunctionParametersContext ctx);
	/**
	 * Visit a parse tree produced by {@link GrammarParser#functionBody}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFunctionBody(GrammarParser.FunctionBodyContext ctx);
	/**
	 * Visit a parse tree produced by {@link GrammarParser#functionRow}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFunctionRow(GrammarParser.FunctionRowContext ctx);
	/**
	 * Visit a parse tree produced by {@link GrammarParser#ifFunction}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIfFunction(GrammarParser.IfFunctionContext ctx);
	/**
	 * Visit a parse tree produced by {@link GrammarParser#whileFunction}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitWhileFunction(GrammarParser.WhileFunctionContext ctx);
	/**
	 * Visit a parse tree produced by {@link GrammarParser#doWhileFunction}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDoWhileFunction(GrammarParser.DoWhileFunctionContext ctx);
	/**
	 * Visit a parse tree produced by {@link GrammarParser#untilFunction}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitUntilFunction(GrammarParser.UntilFunctionContext ctx);
	/**
	 * Visit a parse tree produced by {@link GrammarParser#doUntilFunction}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDoUntilFunction(GrammarParser.DoUntilFunctionContext ctx);
	/**
	 * Visit a parse tree produced by {@link GrammarParser#forFunction}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitForFunction(GrammarParser.ForFunctionContext ctx);
	/**
	 * Visit a parse tree produced by {@link GrammarParser#returnValue}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitReturnValue(GrammarParser.ReturnValueContext ctx);
}