// Generated from C:/Users/HP/IdeaProjects/fjp-semestralka/src/main\Grammar.g4 by ANTLR 4.8
package gen;
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class GrammarParser extends Parser {
    static {
        RuntimeMetaData.checkVersion("4.8", RuntimeMetaData.VERSION);
    }

    protected static final DFA[] _decisionToDFA;
    protected static final PredictionContextCache _sharedContextCache =
            new PredictionContextCache();
    public static final int
            WORD = 1, NUMBER = 2, WHITESPACE = 3, TRUE = 4, FALSE = 5, OPEN_BRACKET = 6, CLOSE_BRACKET = 7,
            OPEN_FUNC_BRACKET = 8, CLOSE_FUNC_BRACKET = 9, COMMA = 10, SEMICOLON = 11, QUATE = 12,
            QUESTION_MARK = 13, COLON = 14, INT = 15, BOOLEAN = 16, VOID = 17, CREATE_VAR = 18,
            CREATE_CONST = 19, ASSIGN = 20, PLUS = 21, MINUS = 22, MULTIPLY = 23, DIVIDE = 24,
            EQUAL = 25, LESS = 26, EQ_LESS = 27, GREATER = 28, EQ_GREATER = 29, NOT_EQUAL = 30,
            AND = 31, OR = 32, NEGATION = 33, FUNCTION_KEYWORD = 34, IF_KEYWORD = 35, WHILE_KEYWORD = 36,
            UNTIL_KEYWORD = 37, DO_KEYWORD = 38, RETURN_KEYWORD = 39, ElSE_KEYWORD = 40, FOR_KEYWORD = 41;
    public static final int
            RULE_entryPoint = 0, RULE_booleanValue = 1, RULE_number = 2, RULE_identifier = 3,
            RULE_dateType = 4, RULE_dateTypeFce = 5, RULE_create = 6, RULE_addOperator = 7,
            RULE_multiplyOperator = 8, RULE_logicalOperator = 9, RULE_logicalOperatorBinary = 10,
            RULE_logicalOperation = 11, RULE_negateLogicalOperation = 12, RULE_logicalOperationInner = 13,
            RULE_numeralOperation = 14, RULE_numeralOperationInner = 15, RULE_numeralOperationFinal = 16,
            RULE_bracketsOperation = 17, RULE_operation = 18, RULE_ternaryOperation = 19,
            RULE_parameters = 20, RULE_parameter = 21, RULE_condition = 22, RULE_defineVariable = 23,
            RULE_multipleIdentifiers = 24, RULE_multipleOperations = 25, RULE_defineFunction = 26,
            RULE_callFunction = 27, RULE_callFunctionParameters = 28, RULE_functionBody = 29,
            RULE_functionRow = 30, RULE_ifFunction = 31, RULE_whileFunction = 32,
            RULE_doWhileFunction = 33, RULE_untilFunction = 34, RULE_doUntilFunction = 35,
            RULE_forFunction = 36, RULE_returnValue = 37;

    private static String[] makeRuleNames() {
        return new String[]{
                "entryPoint", "booleanValue", "number", "identifier", "dateType", "dateTypeFce",
                "create", "addOperator", "multiplyOperator", "logicalOperator", "logicalOperatorBinary",
                "logicalOperation", "negateLogicalOperation", "logicalOperationInner",
                "numeralOperation", "numeralOperationInner", "numeralOperationFinal",
                "bracketsOperation", "operation", "ternaryOperation", "parameters", "parameter",
			"condition", "defineVariable", "multipleIdentifiers", "multipleOperations", 
			"defineFunction", "callFunction", "callFunctionParameters", "functionBody", 
			"functionRow", "ifFunction", "whileFunction", "doWhileFunction", "untilFunction", 
			"doUntilFunction", "forFunction", "returnValue"
		};
	}
	public static final String[] ruleNames = makeRuleNames();

	private static String[] makeLiteralNames() {
        return new String[]{
                null, null, null, null, "'true'", "'false'", "'('", "')'", "'{'", "'}'",
                "','", "';'", "'\"'", "'?'", "':'", "'int'", "'boolean'", "'void'", "'definuj'",
                "'definuj_konstanta'", "'='", "'+'", "'-'", "'*'", "'/'", "'=='", "'<'",
                "'<='", "'>'", "'>='", "'!='", "'&&'", "'||'", "'!'", "'funkce'", "'pokud'",
                "'dokud'", "'dokudNe'", "'opakuj'", "'vrat'", "'jinak'", "'pro'"
        };
    }
	private static final String[] _LITERAL_NAMES = makeLiteralNames();
	private static String[] makeSymbolicNames() {
        return new String[]{
                null, "WORD", "NUMBER", "WHITESPACE", "TRUE", "FALSE", "OPEN_BRACKET",
                "CLOSE_BRACKET", "OPEN_FUNC_BRACKET", "CLOSE_FUNC_BRACKET", "COMMA",
                "SEMICOLON", "QUATE", "QUESTION_MARK", "COLON", "INT", "BOOLEAN", "VOID",
                "CREATE_VAR", "CREATE_CONST", "ASSIGN", "PLUS", "MINUS", "MULTIPLY",
                "DIVIDE", "EQUAL", "LESS", "EQ_LESS", "GREATER", "EQ_GREATER", "NOT_EQUAL",
                "AND", "OR", "NEGATION", "FUNCTION_KEYWORD", "IF_KEYWORD", "WHILE_KEYWORD",
                "UNTIL_KEYWORD", "DO_KEYWORD", "RETURN_KEYWORD", "ElSE_KEYWORD", "FOR_KEYWORD"
        };
    }
	private static final String[] _SYMBOLIC_NAMES = makeSymbolicNames();
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}

	@Override
	public String getGrammarFileName() { return "Grammar.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }

	public GrammarParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}

	public static class EntryPointContext extends ParserRuleContext {
		public List<DefineVariableContext> defineVariable() {
			return getRuleContexts(DefineVariableContext.class);
		}
		public DefineVariableContext defineVariable(int i) {
			return getRuleContext(DefineVariableContext.class,i);
		}
		public List<DefineFunctionContext> defineFunction() {
			return getRuleContexts(DefineFunctionContext.class);
		}
		public DefineFunctionContext defineFunction(int i) {
			return getRuleContext(DefineFunctionContext.class,i);
		}
		public List<WhileFunctionContext> whileFunction() {
			return getRuleContexts(WhileFunctionContext.class);
		}
		public WhileFunctionContext whileFunction(int i) {
			return getRuleContext(WhileFunctionContext.class,i);
		}
		public List<DoWhileFunctionContext> doWhileFunction() {
			return getRuleContexts(DoWhileFunctionContext.class);
		}
		public DoWhileFunctionContext doWhileFunction(int i) {
			return getRuleContext(DoWhileFunctionContext.class,i);
		}
		public List<DoUntilFunctionContext> doUntilFunction() {
			return getRuleContexts(DoUntilFunctionContext.class);
		}
		public DoUntilFunctionContext doUntilFunction(int i) {
			return getRuleContext(DoUntilFunctionContext.class,i);
		}
		public List<IfFunctionContext> ifFunction() {
			return getRuleContexts(IfFunctionContext.class);
		}
		public IfFunctionContext ifFunction(int i) {
			return getRuleContext(IfFunctionContext.class,i);
		}
		public List<UntilFunctionContext> untilFunction() {
			return getRuleContexts(UntilFunctionContext.class);
		}
		public UntilFunctionContext untilFunction(int i) {
			return getRuleContext(UntilFunctionContext.class,i);
		}
		public List<ForFunctionContext> forFunction() {
			return getRuleContexts(ForFunctionContext.class);
		}
		public ForFunctionContext forFunction(int i) {
			return getRuleContext(ForFunctionContext.class,i);
		}
		public List<CallFunctionContext> callFunction() {
			return getRuleContexts(CallFunctionContext.class);
		}
		public CallFunctionContext callFunction(int i) {
			return getRuleContext(CallFunctionContext.class,i);
		}
		public List<TerminalNode> SEMICOLON() { return getTokens(GrammarParser.SEMICOLON); }
		public TerminalNode SEMICOLON(int i) {
			return getToken(GrammarParser.SEMICOLON, i);
		}
		public EntryPointContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_entryPoint; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GrammarListener ) ((GrammarListener)listener).enterEntryPoint(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GrammarListener ) ((GrammarListener)listener).exitEntryPoint(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GrammarVisitor ) return ((GrammarVisitor<? extends T>)visitor).visitEntryPoint(this);
			else return visitor.visitChildren(this);
		}
	}

	public final EntryPointContext entryPoint() throws RecognitionException {
		EntryPointContext _localctx = new EntryPointContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_entryPoint);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(89);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << WORD) | (1L << QUESTION_MARK) | (1L << CREATE_VAR) | (1L << CREATE_CONST) | (1L << FUNCTION_KEYWORD) | (1L << IF_KEYWORD) | (1L << WHILE_KEYWORD) | (1L << UNTIL_KEYWORD) | (1L << DO_KEYWORD) | (1L << FOR_KEYWORD))) != 0)) {
				{
				setState(87);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,0,_ctx) ) {
				case 1:
					{
					setState(76);
					defineVariable();
					}
					break;
				case 2:
					{
					setState(77);
					defineFunction();
					}
					break;
				case 3:
					{
					{
					setState(78);
					callFunction();
					setState(79);
					match(SEMICOLON);
					}
					}
					break;
				case 4:
					{
					setState(81);
					whileFunction();
					}
					break;
				case 5:
					{
					setState(82);
					doWhileFunction();
					}
					break;
				case 6:
					{
					setState(83);
					doUntilFunction();
					}
					break;
				case 7:
					{
					setState(84);
					ifFunction();
					}
					break;
				case 8:
					{
					setState(85);
					untilFunction();
					}
					break;
				case 9:
					{
					setState(86);
					forFunction();
					}
					break;
				}
				}
				setState(91);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class BooleanValueContext extends ParserRuleContext {
		public TerminalNode TRUE() { return getToken(GrammarParser.TRUE, 0); }
		public TerminalNode FALSE() { return getToken(GrammarParser.FALSE, 0); }
		public BooleanValueContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_booleanValue; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GrammarListener ) ((GrammarListener)listener).enterBooleanValue(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GrammarListener ) ((GrammarListener)listener).exitBooleanValue(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GrammarVisitor ) return ((GrammarVisitor<? extends T>)visitor).visitBooleanValue(this);
			else return visitor.visitChildren(this);
		}
	}

	public final BooleanValueContext booleanValue() throws RecognitionException {
		BooleanValueContext _localctx = new BooleanValueContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_booleanValue);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(92);
			_la = _input.LA(1);
			if ( !(_la==TRUE || _la==FALSE) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class NumberContext extends ParserRuleContext {
		public List<TerminalNode> NUMBER() { return getTokens(GrammarParser.NUMBER); }
		public TerminalNode NUMBER(int i) {
			return getToken(GrammarParser.NUMBER, i);
		}
		public TerminalNode MINUS() { return getToken(GrammarParser.MINUS, 0); }
		public NumberContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_number; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GrammarListener ) ((GrammarListener)listener).enterNumber(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GrammarListener ) ((GrammarListener)listener).exitNumber(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GrammarVisitor ) return ((GrammarVisitor<? extends T>)visitor).visitNumber(this);
			else return visitor.visitChildren(this);
		}
	}

	public final NumberContext number() throws RecognitionException {
		NumberContext _localctx = new NumberContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_number);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(95);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==MINUS) {
				{
				setState(94);
				match(MINUS);
				}
			}

			setState(97);
			match(NUMBER);
			setState(101);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,3,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(98);
					match(NUMBER);
					}
					} 
				}
				setState(103);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,3,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class IdentifierContext extends ParserRuleContext {
		public List<TerminalNode> WORD() { return getTokens(GrammarParser.WORD); }
		public TerminalNode WORD(int i) {
			return getToken(GrammarParser.WORD, i);
		}
		public IdentifierContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_identifier; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GrammarListener ) ((GrammarListener)listener).enterIdentifier(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GrammarListener ) ((GrammarListener)listener).exitIdentifier(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GrammarVisitor ) return ((GrammarVisitor<? extends T>)visitor).visitIdentifier(this);
			else return visitor.visitChildren(this);
		}
	}

	public final IdentifierContext identifier() throws RecognitionException {
		IdentifierContext _localctx = new IdentifierContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_identifier);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(104);
			match(WORD);
			setState(108);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,4,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(105);
					match(WORD);
					}
					} 
				}
				setState(110);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,4,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class DateTypeContext extends ParserRuleContext {
		public TerminalNode INT() { return getToken(GrammarParser.INT, 0); }
		public TerminalNode BOOLEAN() { return getToken(GrammarParser.BOOLEAN, 0); }
		public DateTypeContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_dateType; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GrammarListener ) ((GrammarListener)listener).enterDateType(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GrammarListener ) ((GrammarListener)listener).exitDateType(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GrammarVisitor ) return ((GrammarVisitor<? extends T>)visitor).visitDateType(this);
			else return visitor.visitChildren(this);
		}
	}

	public final DateTypeContext dateType() throws RecognitionException {
		DateTypeContext _localctx = new DateTypeContext(_ctx, getState());
		enterRule(_localctx, 8, RULE_dateType);
		int _la;
		try {
            enterOuterAlt(_localctx, 1);
            {
                setState(111);
                _la = _input.LA(1);
                if (!(_la == INT || _la == BOOLEAN)) {
                    _errHandler.recoverInline(this);
                } else {
                    if (_input.LA(1) == Token.EOF) matchedEOF = true;
                    _errHandler.reportMatch(this);
                    consume();
                }
            }
        }
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class DateTypeFceContext extends ParserRuleContext {
		public DateTypeContext dateType() {
			return getRuleContext(DateTypeContext.class,0);
		}
		public TerminalNode VOID() { return getToken(GrammarParser.VOID, 0); }
		public DateTypeFceContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_dateTypeFce; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GrammarListener ) ((GrammarListener)listener).enterDateTypeFce(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GrammarListener ) ((GrammarListener)listener).exitDateTypeFce(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GrammarVisitor ) return ((GrammarVisitor<? extends T>)visitor).visitDateTypeFce(this);
			else return visitor.visitChildren(this);
		}
	}

	public final DateTypeFceContext dateTypeFce() throws RecognitionException {
		DateTypeFceContext _localctx = new DateTypeFceContext(_ctx, getState());
		enterRule(_localctx, 10, RULE_dateTypeFce);
		try {
			setState(115);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case INT:
			case BOOLEAN:
				enterOuterAlt(_localctx, 1);
				{
				setState(113);
				dateType();
				}
				break;
			case VOID:
				enterOuterAlt(_localctx, 2);
				{
				setState(114);
				match(VOID);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class CreateContext extends ParserRuleContext {
		public TerminalNode CREATE_VAR() { return getToken(GrammarParser.CREATE_VAR, 0); }
		public TerminalNode CREATE_CONST() { return getToken(GrammarParser.CREATE_CONST, 0); }
		public CreateContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_create; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GrammarListener ) ((GrammarListener)listener).enterCreate(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GrammarListener ) ((GrammarListener)listener).exitCreate(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GrammarVisitor ) return ((GrammarVisitor<? extends T>)visitor).visitCreate(this);
			else return visitor.visitChildren(this);
		}
	}

	public final CreateContext create() throws RecognitionException {
		CreateContext _localctx = new CreateContext(_ctx, getState());
		enterRule(_localctx, 12, RULE_create);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(117);
			_la = _input.LA(1);
			if ( !(_la==CREATE_VAR || _la==CREATE_CONST) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class AddOperatorContext extends ParserRuleContext {
		public TerminalNode PLUS() { return getToken(GrammarParser.PLUS, 0); }
		public TerminalNode MINUS() { return getToken(GrammarParser.MINUS, 0); }
		public AddOperatorContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_addOperator; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GrammarListener ) ((GrammarListener)listener).enterAddOperator(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GrammarListener ) ((GrammarListener)listener).exitAddOperator(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GrammarVisitor ) return ((GrammarVisitor<? extends T>)visitor).visitAddOperator(this);
			else return visitor.visitChildren(this);
		}
	}

	public final AddOperatorContext addOperator() throws RecognitionException {
		AddOperatorContext _localctx = new AddOperatorContext(_ctx, getState());
		enterRule(_localctx, 14, RULE_addOperator);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(119);
			_la = _input.LA(1);
			if ( !(_la==PLUS || _la==MINUS) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class MultiplyOperatorContext extends ParserRuleContext {
		public TerminalNode MULTIPLY() { return getToken(GrammarParser.MULTIPLY, 0); }
		public TerminalNode DIVIDE() { return getToken(GrammarParser.DIVIDE, 0); }
		public MultiplyOperatorContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_multiplyOperator; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GrammarListener ) ((GrammarListener)listener).enterMultiplyOperator(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GrammarListener ) ((GrammarListener)listener).exitMultiplyOperator(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GrammarVisitor ) return ((GrammarVisitor<? extends T>)visitor).visitMultiplyOperator(this);
			else return visitor.visitChildren(this);
		}
	}

	public final MultiplyOperatorContext multiplyOperator() throws RecognitionException {
		MultiplyOperatorContext _localctx = new MultiplyOperatorContext(_ctx, getState());
		enterRule(_localctx, 16, RULE_multiplyOperator);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(121);
			_la = _input.LA(1);
			if ( !(_la==MULTIPLY || _la==DIVIDE) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class LogicalOperatorContext extends ParserRuleContext {
		public TerminalNode EQUAL() { return getToken(GrammarParser.EQUAL, 0); }
		public TerminalNode LESS() { return getToken(GrammarParser.LESS, 0); }
		public TerminalNode EQ_LESS() { return getToken(GrammarParser.EQ_LESS, 0); }
		public TerminalNode GREATER() { return getToken(GrammarParser.GREATER, 0); }
		public TerminalNode EQ_GREATER() { return getToken(GrammarParser.EQ_GREATER, 0); }
		public TerminalNode NOT_EQUAL() { return getToken(GrammarParser.NOT_EQUAL, 0); }
		public LogicalOperatorContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_logicalOperator; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GrammarListener ) ((GrammarListener)listener).enterLogicalOperator(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GrammarListener ) ((GrammarListener)listener).exitLogicalOperator(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GrammarVisitor ) return ((GrammarVisitor<? extends T>)visitor).visitLogicalOperator(this);
			else return visitor.visitChildren(this);
		}
	}

	public final LogicalOperatorContext logicalOperator() throws RecognitionException {
		LogicalOperatorContext _localctx = new LogicalOperatorContext(_ctx, getState());
		enterRule(_localctx, 18, RULE_logicalOperator);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(123);
			_la = _input.LA(1);
			if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << EQUAL) | (1L << LESS) | (1L << EQ_LESS) | (1L << GREATER) | (1L << EQ_GREATER) | (1L << NOT_EQUAL))) != 0)) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class LogicalOperatorBinaryContext extends ParserRuleContext {
		public TerminalNode AND() { return getToken(GrammarParser.AND, 0); }
		public TerminalNode OR() { return getToken(GrammarParser.OR, 0); }
		public LogicalOperatorBinaryContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_logicalOperatorBinary; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GrammarListener ) ((GrammarListener)listener).enterLogicalOperatorBinary(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GrammarListener ) ((GrammarListener)listener).exitLogicalOperatorBinary(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GrammarVisitor ) return ((GrammarVisitor<? extends T>)visitor).visitLogicalOperatorBinary(this);
			else return visitor.visitChildren(this);
		}
	}

	public final LogicalOperatorBinaryContext logicalOperatorBinary() throws RecognitionException {
		LogicalOperatorBinaryContext _localctx = new LogicalOperatorBinaryContext(_ctx, getState());
		enterRule(_localctx, 20, RULE_logicalOperatorBinary);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(125);
			_la = _input.LA(1);
			if ( !(_la==AND || _la==OR) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class LogicalOperationContext extends ParserRuleContext {
		public List<NegateLogicalOperationContext> negateLogicalOperation() {
			return getRuleContexts(NegateLogicalOperationContext.class);
		}
		public NegateLogicalOperationContext negateLogicalOperation(int i) {
			return getRuleContext(NegateLogicalOperationContext.class,i);
		}
		public List<LogicalOperatorBinaryContext> logicalOperatorBinary() {
			return getRuleContexts(LogicalOperatorBinaryContext.class);
		}
		public LogicalOperatorBinaryContext logicalOperatorBinary(int i) {
			return getRuleContext(LogicalOperatorBinaryContext.class,i);
		}
		public LogicalOperationContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_logicalOperation; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GrammarListener ) ((GrammarListener)listener).enterLogicalOperation(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GrammarListener ) ((GrammarListener)listener).exitLogicalOperation(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GrammarVisitor ) return ((GrammarVisitor<? extends T>)visitor).visitLogicalOperation(this);
			else return visitor.visitChildren(this);
		}
	}

	public final LogicalOperationContext logicalOperation() throws RecognitionException {
		LogicalOperationContext _localctx = new LogicalOperationContext(_ctx, getState());
		enterRule(_localctx, 22, RULE_logicalOperation);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(127);
			negateLogicalOperation();
			setState(133);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==AND || _la==OR) {
				{
				{
				setState(128);
				logicalOperatorBinary();
				setState(129);
				negateLogicalOperation();
				}
				}
				setState(135);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class NegateLogicalOperationContext extends ParserRuleContext {
		public LogicalOperationInnerContext logicalOperationInner() {
			return getRuleContext(LogicalOperationInnerContext.class,0);
		}
		public TerminalNode NEGATION() { return getToken(GrammarParser.NEGATION, 0); }
		public NegateLogicalOperationContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_negateLogicalOperation; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GrammarListener ) ((GrammarListener)listener).enterNegateLogicalOperation(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GrammarListener ) ((GrammarListener)listener).exitNegateLogicalOperation(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GrammarVisitor ) return ((GrammarVisitor<? extends T>)visitor).visitNegateLogicalOperation(this);
			else return visitor.visitChildren(this);
		}
	}

	public final NegateLogicalOperationContext negateLogicalOperation() throws RecognitionException {
		NegateLogicalOperationContext _localctx = new NegateLogicalOperationContext(_ctx, getState());
		enterRule(_localctx, 24, RULE_negateLogicalOperation);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(137);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==NEGATION) {
				{
				setState(136);
				match(NEGATION);
				}
			}

			{
			setState(139);
			logicalOperationInner();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class LogicalOperationInnerContext extends ParserRuleContext {
		public IdentifierContext identifier() {
			return getRuleContext(IdentifierContext.class,0);
		}
		public BooleanValueContext booleanValue() {
			return getRuleContext(BooleanValueContext.class,0);
		}
		public List<NumeralOperationContext> numeralOperation() {
			return getRuleContexts(NumeralOperationContext.class);
		}
		public NumeralOperationContext numeralOperation(int i) {
			return getRuleContext(NumeralOperationContext.class,i);
		}
		public LogicalOperatorContext logicalOperator() {
			return getRuleContext(LogicalOperatorContext.class,0);
		}
		public LogicalOperationInnerContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_logicalOperationInner; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GrammarListener ) ((GrammarListener)listener).enterLogicalOperationInner(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GrammarListener ) ((GrammarListener)listener).exitLogicalOperationInner(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GrammarVisitor ) return ((GrammarVisitor<? extends T>)visitor).visitLogicalOperationInner(this);
			else return visitor.visitChildren(this);
		}
	}

	public final LogicalOperationInnerContext logicalOperationInner() throws RecognitionException {
		LogicalOperationInnerContext _localctx = new LogicalOperationInnerContext(_ctx, getState());
		enterRule(_localctx, 26, RULE_logicalOperationInner);
		try {
			setState(147);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,8,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(141);
				identifier();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(142);
				booleanValue();
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(143);
				numeralOperation(0);
				setState(144);
				logicalOperator();
				setState(145);
				numeralOperation(0);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class NumeralOperationContext extends ParserRuleContext {
		public NumeralOperationInnerContext numeralOperationInner() {
			return getRuleContext(NumeralOperationInnerContext.class,0);
		}
		public NumeralOperationContext numeralOperation() {
			return getRuleContext(NumeralOperationContext.class,0);
		}
		public AddOperatorContext addOperator() {
			return getRuleContext(AddOperatorContext.class,0);
		}
		public NumeralOperationContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_numeralOperation; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GrammarListener ) ((GrammarListener)listener).enterNumeralOperation(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GrammarListener ) ((GrammarListener)listener).exitNumeralOperation(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GrammarVisitor ) return ((GrammarVisitor<? extends T>)visitor).visitNumeralOperation(this);
			else return visitor.visitChildren(this);
		}
	}

	public final NumeralOperationContext numeralOperation() throws RecognitionException {
		return numeralOperation(0);
	}

	private NumeralOperationContext numeralOperation(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		NumeralOperationContext _localctx = new NumeralOperationContext(_ctx, _parentState);
		NumeralOperationContext _prevctx = _localctx;
		int _startState = 28;
		enterRecursionRule(_localctx, 28, RULE_numeralOperation, _p);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			{
			setState(150);
			numeralOperationInner(0);
			}
			_ctx.stop = _input.LT(-1);
			setState(158);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,9,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					{
					_localctx = new NumeralOperationContext(_parentctx, _parentState);
					pushNewRecursionContext(_localctx, _startState, RULE_numeralOperation);
					setState(152);
					if (!(precpred(_ctx, 2))) throw new FailedPredicateException(this, "precpred(_ctx, 2)");
					setState(153);
					addOperator();
					setState(154);
					numeralOperationInner(0);
					}
					} 
				}
				setState(160);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,9,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class NumeralOperationInnerContext extends ParserRuleContext {
		public NumeralOperationFinalContext numeralOperationFinal() {
			return getRuleContext(NumeralOperationFinalContext.class,0);
		}
		public NumeralOperationInnerContext numeralOperationInner() {
			return getRuleContext(NumeralOperationInnerContext.class,0);
		}
		public MultiplyOperatorContext multiplyOperator() {
			return getRuleContext(MultiplyOperatorContext.class,0);
		}
		public NumeralOperationInnerContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_numeralOperationInner; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GrammarListener ) ((GrammarListener)listener).enterNumeralOperationInner(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GrammarListener ) ((GrammarListener)listener).exitNumeralOperationInner(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GrammarVisitor ) return ((GrammarVisitor<? extends T>)visitor).visitNumeralOperationInner(this);
			else return visitor.visitChildren(this);
		}
	}

	public final NumeralOperationInnerContext numeralOperationInner() throws RecognitionException {
		return numeralOperationInner(0);
	}

	private NumeralOperationInnerContext numeralOperationInner(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		NumeralOperationInnerContext _localctx = new NumeralOperationInnerContext(_ctx, _parentState);
		NumeralOperationInnerContext _prevctx = _localctx;
		int _startState = 30;
		enterRecursionRule(_localctx, 30, RULE_numeralOperationInner, _p);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			{
			setState(162);
			numeralOperationFinal();
			}
			_ctx.stop = _input.LT(-1);
			setState(170);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,10,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					{
					_localctx = new NumeralOperationInnerContext(_parentctx, _parentState);
					pushNewRecursionContext(_localctx, _startState, RULE_numeralOperationInner);
					setState(164);
					if (!(precpred(_ctx, 2))) throw new FailedPredicateException(this, "precpred(_ctx, 2)");
					setState(165);
					multiplyOperator();
					setState(166);
					numeralOperationFinal();
					}
					} 
				}
				setState(172);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,10,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class NumeralOperationFinalContext extends ParserRuleContext {
		public IdentifierContext identifier() {
			return getRuleContext(IdentifierContext.class,0);
		}
		public NumberContext number() {
			return getRuleContext(NumberContext.class,0);
		}
		public BracketsOperationContext bracketsOperation() {
			return getRuleContext(BracketsOperationContext.class,0);
		}
		public NumeralOperationFinalContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_numeralOperationFinal; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GrammarListener ) ((GrammarListener)listener).enterNumeralOperationFinal(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GrammarListener ) ((GrammarListener)listener).exitNumeralOperationFinal(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GrammarVisitor ) return ((GrammarVisitor<? extends T>)visitor).visitNumeralOperationFinal(this);
			else return visitor.visitChildren(this);
		}
	}

	public final NumeralOperationFinalContext numeralOperationFinal() throws RecognitionException {
		NumeralOperationFinalContext _localctx = new NumeralOperationFinalContext(_ctx, getState());
		enterRule(_localctx, 32, RULE_numeralOperationFinal);
		try {
			setState(176);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case WORD:
				enterOuterAlt(_localctx, 1);
				{
				setState(173);
				identifier();
				}
				break;
			case NUMBER:
			case MINUS:
				enterOuterAlt(_localctx, 2);
				{
				setState(174);
				number();
				}
				break;
			case OPEN_BRACKET:
				enterOuterAlt(_localctx, 3);
				{
				setState(175);
				bracketsOperation();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class BracketsOperationContext extends ParserRuleContext {
		public TerminalNode OPEN_BRACKET() { return getToken(GrammarParser.OPEN_BRACKET, 0); }
		public NumeralOperationContext numeralOperation() {
			return getRuleContext(NumeralOperationContext.class,0);
		}
		public TerminalNode CLOSE_BRACKET() { return getToken(GrammarParser.CLOSE_BRACKET, 0); }
		public BracketsOperationContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_bracketsOperation; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GrammarListener ) ((GrammarListener)listener).enterBracketsOperation(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GrammarListener ) ((GrammarListener)listener).exitBracketsOperation(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GrammarVisitor ) return ((GrammarVisitor<? extends T>)visitor).visitBracketsOperation(this);
			else return visitor.visitChildren(this);
		}
	}

	public final BracketsOperationContext bracketsOperation() throws RecognitionException {
		BracketsOperationContext _localctx = new BracketsOperationContext(_ctx, getState());
		enterRule(_localctx, 34, RULE_bracketsOperation);
		try {
			enterOuterAlt(_localctx, 1);
			{
			{
			setState(178);
			match(OPEN_BRACKET);
			setState(179);
			numeralOperation(0);
			setState(180);
			match(CLOSE_BRACKET);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class OperationContext extends ParserRuleContext {
		public NumeralOperationContext numeralOperation() {
			return getRuleContext(NumeralOperationContext.class,0);
		}
		public LogicalOperationContext logicalOperation() {
			return getRuleContext(LogicalOperationContext.class,0);
		}
		public CallFunctionContext callFunction() {
			return getRuleContext(CallFunctionContext.class,0);
		}
		public TernaryOperationContext ternaryOperation() {
			return getRuleContext(TernaryOperationContext.class,0);
		}
		public OperationContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_operation; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GrammarListener ) ((GrammarListener)listener).enterOperation(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GrammarListener ) ((GrammarListener)listener).exitOperation(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GrammarVisitor ) return ((GrammarVisitor<? extends T>)visitor).visitOperation(this);
			else return visitor.visitChildren(this);
		}
	}

	public final OperationContext operation() throws RecognitionException {
		OperationContext _localctx = new OperationContext(_ctx, getState());
		enterRule(_localctx, 36, RULE_operation);
		try {
			setState(186);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,12,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(182);
				numeralOperation(0);
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(183);
				logicalOperation();
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(184);
				callFunction();
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(185);
				ternaryOperation();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class TernaryOperationContext extends ParserRuleContext {
		public ConditionContext condition() {
			return getRuleContext(ConditionContext.class,0);
		}
		public TerminalNode QUESTION_MARK() { return getToken(GrammarParser.QUESTION_MARK, 0); }
		public List<OperationContext> operation() {
			return getRuleContexts(OperationContext.class);
		}
		public OperationContext operation(int i) {
			return getRuleContext(OperationContext.class,i);
		}
		public TerminalNode COLON() { return getToken(GrammarParser.COLON, 0); }
		public TernaryOperationContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_ternaryOperation; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GrammarListener ) ((GrammarListener)listener).enterTernaryOperation(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GrammarListener ) ((GrammarListener)listener).exitTernaryOperation(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GrammarVisitor ) return ((GrammarVisitor<? extends T>)visitor).visitTernaryOperation(this);
			else return visitor.visitChildren(this);
		}
	}

	public final TernaryOperationContext ternaryOperation() throws RecognitionException {
		TernaryOperationContext _localctx = new TernaryOperationContext(_ctx, getState());
		enterRule(_localctx, 38, RULE_ternaryOperation);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(188);
			condition();
			setState(189);
			match(QUESTION_MARK);
			setState(190);
			operation();
			setState(191);
			match(COLON);
			setState(192);
			operation();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ParametersContext extends ParserRuleContext {
		public TerminalNode OPEN_BRACKET() { return getToken(GrammarParser.OPEN_BRACKET, 0); }
		public TerminalNode CLOSE_BRACKET() { return getToken(GrammarParser.CLOSE_BRACKET, 0); }
		public List<ParameterContext> parameter() {
			return getRuleContexts(ParameterContext.class);
		}
		public ParameterContext parameter(int i) {
			return getRuleContext(ParameterContext.class,i);
		}
		public List<TerminalNode> COMMA() { return getTokens(GrammarParser.COMMA); }
		public TerminalNode COMMA(int i) {
			return getToken(GrammarParser.COMMA, i);
		}
		public ParametersContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_parameters; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GrammarListener ) ((GrammarListener)listener).enterParameters(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GrammarListener ) ((GrammarListener)listener).exitParameters(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GrammarVisitor ) return ((GrammarVisitor<? extends T>)visitor).visitParameters(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ParametersContext parameters() throws RecognitionException {
		ParametersContext _localctx = new ParametersContext(_ctx, getState());
		enterRule(_localctx, 40, RULE_parameters);
		int _la;
		try {
            enterOuterAlt(_localctx, 1);
            {
                setState(194);
                match(OPEN_BRACKET);
                setState(203);
                _errHandler.sync(this);
                _la = _input.LA(1);
                if (_la == INT || _la == BOOLEAN) {
                    {
                        setState(195);
                        parameter();
                        setState(200);
                        _errHandler.sync(this);
                        _la = _input.LA(1);
                        while (_la == COMMA) {
                            {
                                {
                                    setState(196);
                                    match(COMMA);
                                    setState(197);
                                    parameter();
                                }
                            }
                            setState(202);
                            _errHandler.sync(this);
                            _la = _input.LA(1);
                        }
                    }
			}

			setState(205);
			match(CLOSE_BRACKET);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ParameterContext extends ParserRuleContext {
		public DateTypeContext dateType() {
			return getRuleContext(DateTypeContext.class,0);
		}
		public IdentifierContext identifier() {
			return getRuleContext(IdentifierContext.class,0);
		}
		public ParameterContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_parameter; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GrammarListener ) ((GrammarListener)listener).enterParameter(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GrammarListener ) ((GrammarListener)listener).exitParameter(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GrammarVisitor ) return ((GrammarVisitor<? extends T>)visitor).visitParameter(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ParameterContext parameter() throws RecognitionException {
		ParameterContext _localctx = new ParameterContext(_ctx, getState());
		enterRule(_localctx, 42, RULE_parameter);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(207);
			dateType();
			setState(208);
			identifier();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ConditionContext extends ParserRuleContext {
		public TerminalNode OPEN_BRACKET() { return getToken(GrammarParser.OPEN_BRACKET, 0); }
		public LogicalOperationContext logicalOperation() {
			return getRuleContext(LogicalOperationContext.class,0);
		}
		public TerminalNode CLOSE_BRACKET() { return getToken(GrammarParser.CLOSE_BRACKET, 0); }
		public ConditionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_condition; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GrammarListener ) ((GrammarListener)listener).enterCondition(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GrammarListener ) ((GrammarListener)listener).exitCondition(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GrammarVisitor ) return ((GrammarVisitor<? extends T>)visitor).visitCondition(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ConditionContext condition() throws RecognitionException {
		ConditionContext _localctx = new ConditionContext(_ctx, getState());
		enterRule(_localctx, 44, RULE_condition);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(210);
			match(OPEN_BRACKET);
			setState(211);
			logicalOperation();
			setState(212);
			match(CLOSE_BRACKET);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class DefineVariableContext extends ParserRuleContext {
		public CreateContext create() {
			return getRuleContext(CreateContext.class,0);
		}
		public DateTypeContext dateType() {
			return getRuleContext(DateTypeContext.class,0);
		}
		public List<IdentifierContext> identifier() {
			return getRuleContexts(IdentifierContext.class);
		}
		public IdentifierContext identifier(int i) {
			return getRuleContext(IdentifierContext.class,i);
		}
		public List<TerminalNode> ASSIGN() { return getTokens(GrammarParser.ASSIGN); }
		public TerminalNode ASSIGN(int i) {
			return getToken(GrammarParser.ASSIGN, i);
		}
		public OperationContext operation() {
			return getRuleContext(OperationContext.class,0);
		}
		public TerminalNode SEMICOLON() { return getToken(GrammarParser.SEMICOLON, 0); }
		public MultipleIdentifiersContext multipleIdentifiers() {
			return getRuleContext(MultipleIdentifiersContext.class,0);
		}
		public MultipleOperationsContext multipleOperations() {
			return getRuleContext(MultipleOperationsContext.class,0);
		}
		public DefineVariableContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_defineVariable; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GrammarListener ) ((GrammarListener)listener).enterDefineVariable(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GrammarListener ) ((GrammarListener)listener).exitDefineVariable(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GrammarVisitor ) return ((GrammarVisitor<? extends T>)visitor).visitDefineVariable(this);
			else return visitor.visitChildren(this);
		}
	}

	public final DefineVariableContext defineVariable() throws RecognitionException {
		DefineVariableContext _localctx = new DefineVariableContext(_ctx, getState());
		enterRule(_localctx, 46, RULE_defineVariable);
		try {
			int _alt;
			setState(236);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,16,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(214);
				create();
				setState(215);
				dateType();
				setState(216);
				identifier();
				setState(217);
				match(ASSIGN);
				setState(223);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,15,_ctx);
				while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
					if ( _alt==1 ) {
						{
						{
						setState(218);
						identifier();
						setState(219);
						match(ASSIGN);
						}
						} 
					}
					setState(225);
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,15,_ctx);
				}
				setState(226);
				operation();
				setState(227);
				match(SEMICOLON);
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(229);
				create();
				setState(230);
				dateType();
				setState(231);
				multipleIdentifiers();
				setState(232);
				match(ASSIGN);
				setState(233);
				multipleOperations();
				setState(234);
				match(SEMICOLON);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class MultipleIdentifiersContext extends ParserRuleContext {
		public TerminalNode OPEN_FUNC_BRACKET() { return getToken(GrammarParser.OPEN_FUNC_BRACKET, 0); }
		public List<IdentifierContext> identifier() {
			return getRuleContexts(IdentifierContext.class);
		}
		public IdentifierContext identifier(int i) {
			return getRuleContext(IdentifierContext.class,i);
		}
		public TerminalNode CLOSE_FUNC_BRACKET() { return getToken(GrammarParser.CLOSE_FUNC_BRACKET, 0); }
		public List<TerminalNode> COMMA() { return getTokens(GrammarParser.COMMA); }
		public TerminalNode COMMA(int i) {
			return getToken(GrammarParser.COMMA, i);
		}
		public MultipleIdentifiersContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_multipleIdentifiers; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GrammarListener ) ((GrammarListener)listener).enterMultipleIdentifiers(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GrammarListener ) ((GrammarListener)listener).exitMultipleIdentifiers(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GrammarVisitor ) return ((GrammarVisitor<? extends T>)visitor).visitMultipleIdentifiers(this);
			else return visitor.visitChildren(this);
		}
	}

	public final MultipleIdentifiersContext multipleIdentifiers() throws RecognitionException {
		MultipleIdentifiersContext _localctx = new MultipleIdentifiersContext(_ctx, getState());
		enterRule(_localctx, 48, RULE_multipleIdentifiers);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(238);
			match(OPEN_FUNC_BRACKET);
			setState(239);
			identifier();
			setState(242); 
			_errHandler.sync(this);
			_la = _input.LA(1);
			do {
				{
				{
				setState(240);
				match(COMMA);
				setState(241);
				identifier();
				}
				}
				setState(244); 
				_errHandler.sync(this);
				_la = _input.LA(1);
			} while ( _la==COMMA );
			setState(246);
			match(CLOSE_FUNC_BRACKET);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class MultipleOperationsContext extends ParserRuleContext {
		public TerminalNode OPEN_FUNC_BRACKET() { return getToken(GrammarParser.OPEN_FUNC_BRACKET, 0); }
		public List<OperationContext> operation() {
			return getRuleContexts(OperationContext.class);
		}
		public OperationContext operation(int i) {
			return getRuleContext(OperationContext.class,i);
		}
		public TerminalNode CLOSE_FUNC_BRACKET() { return getToken(GrammarParser.CLOSE_FUNC_BRACKET, 0); }
		public List<TerminalNode> COMMA() { return getTokens(GrammarParser.COMMA); }
		public TerminalNode COMMA(int i) {
			return getToken(GrammarParser.COMMA, i);
		}
		public MultipleOperationsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_multipleOperations; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GrammarListener ) ((GrammarListener)listener).enterMultipleOperations(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GrammarListener ) ((GrammarListener)listener).exitMultipleOperations(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GrammarVisitor ) return ((GrammarVisitor<? extends T>)visitor).visitMultipleOperations(this);
			else return visitor.visitChildren(this);
		}
	}

	public final MultipleOperationsContext multipleOperations() throws RecognitionException {
		MultipleOperationsContext _localctx = new MultipleOperationsContext(_ctx, getState());
		enterRule(_localctx, 50, RULE_multipleOperations);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(248);
			match(OPEN_FUNC_BRACKET);
			setState(249);
			operation();
			setState(252); 
			_errHandler.sync(this);
			_la = _input.LA(1);
			do {
				{
				{
				setState(250);
				match(COMMA);
				setState(251);
				operation();
				}
				}
				setState(254); 
				_errHandler.sync(this);
				_la = _input.LA(1);
			} while ( _la==COMMA );
			setState(256);
			match(CLOSE_FUNC_BRACKET);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class DefineFunctionContext extends ParserRuleContext {
		public TerminalNode FUNCTION_KEYWORD() { return getToken(GrammarParser.FUNCTION_KEYWORD, 0); }
		public DateTypeFceContext dateTypeFce() {
			return getRuleContext(DateTypeFceContext.class,0);
		}
		public IdentifierContext identifier() {
			return getRuleContext(IdentifierContext.class,0);
		}
		public ParametersContext parameters() {
			return getRuleContext(ParametersContext.class,0);
		}
		public FunctionBodyContext functionBody() {
			return getRuleContext(FunctionBodyContext.class,0);
		}
		public DefineFunctionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_defineFunction; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GrammarListener ) ((GrammarListener)listener).enterDefineFunction(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GrammarListener ) ((GrammarListener)listener).exitDefineFunction(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GrammarVisitor ) return ((GrammarVisitor<? extends T>)visitor).visitDefineFunction(this);
			else return visitor.visitChildren(this);
		}
	}

	public final DefineFunctionContext defineFunction() throws RecognitionException {
		DefineFunctionContext _localctx = new DefineFunctionContext(_ctx, getState());
		enterRule(_localctx, 52, RULE_defineFunction);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(258);
			match(FUNCTION_KEYWORD);
			setState(259);
			dateTypeFce();
			setState(260);
			identifier();
			setState(261);
			parameters();
			setState(262);
			functionBody();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class CallFunctionContext extends ParserRuleContext {
		public IdentifierContext identifier() {
			return getRuleContext(IdentifierContext.class,0);
		}
		public CallFunctionParametersContext callFunctionParameters() {
			return getRuleContext(CallFunctionParametersContext.class,0);
		}
		public CallFunctionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_callFunction; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GrammarListener ) ((GrammarListener)listener).enterCallFunction(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GrammarListener ) ((GrammarListener)listener).exitCallFunction(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GrammarVisitor ) return ((GrammarVisitor<? extends T>)visitor).visitCallFunction(this);
			else return visitor.visitChildren(this);
		}
	}

	public final CallFunctionContext callFunction() throws RecognitionException {
		CallFunctionContext _localctx = new CallFunctionContext(_ctx, getState());
		enterRule(_localctx, 54, RULE_callFunction);
        try {
            enterOuterAlt(_localctx, 1);
            {
                setState(264);
                identifier();
                setState(265);
                callFunctionParameters();
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public static class CallFunctionParametersContext extends ParserRuleContext {
        public TerminalNode OPEN_BRACKET() {
            return getToken(GrammarParser.OPEN_BRACKET, 0);
        }

        public TerminalNode CLOSE_BRACKET() {
            return getToken(GrammarParser.CLOSE_BRACKET, 0);
        }

        public List<IdentifierContext> identifier() {
            return getRuleContexts(IdentifierContext.class);
        }

        public IdentifierContext identifier(int i) {
            return getRuleContext(IdentifierContext.class, i);
        }

        public List<BooleanValueContext> booleanValue() {
            return getRuleContexts(BooleanValueContext.class);
        }

        public BooleanValueContext booleanValue(int i) {
            return getRuleContext(BooleanValueContext.class, i);
        }

        public List<NumberContext> number() {
            return getRuleContexts(NumberContext.class);
        }

        public NumberContext number(int i) {
            return getRuleContext(NumberContext.class, i);
        }

        public List<TerminalNode> COMMA() {
            return getTokens(GrammarParser.COMMA);
        }

        public TerminalNode COMMA(int i) {
            return getToken(GrammarParser.COMMA, i);
        }

        public CallFunctionParametersContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        @Override
        public int getRuleIndex() {
            return RULE_callFunctionParameters;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof GrammarListener) ((GrammarListener) listener).enterCallFunctionParameters(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof GrammarListener) ((GrammarListener) listener).exitCallFunctionParameters(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof GrammarVisitor)
                return ((GrammarVisitor<? extends T>) visitor).visitCallFunctionParameters(this);
            else return visitor.visitChildren(this);
        }
    }

    public final CallFunctionParametersContext callFunctionParameters() throws RecognitionException {
        CallFunctionParametersContext _localctx = new CallFunctionParametersContext(_ctx, getState());
        enterRule(_localctx, 56, RULE_callFunctionParameters);
        int _la;
        try {
            enterOuterAlt(_localctx, 1);
            {
                setState(267);
                match(OPEN_BRACKET);
                setState(284);
                _errHandler.sync(this);
                _la = _input.LA(1);
                if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << WORD) | (1L << NUMBER) | (1L << TRUE) | (1L << FALSE) | (1L << MINUS))) != 0)) {
                    {
                        setState(271);
                        _errHandler.sync(this);
                        switch (_input.LA(1)) {
                            case WORD: {
                                setState(268);
                                identifier();
                            }
                            break;
                            case TRUE:
                            case FALSE: {
                                setState(269);
                                booleanValue();
                            }
                            break;
                            case NUMBER:
                            case MINUS: {
                                setState(270);
                                number();
                            }
                            break;
                            default:
                                throw new NoViableAltException(this);
                        }
                        setState(281);
                        _errHandler.sync(this);
                        _la = _input.LA(1);
                        while (_la == COMMA) {
                            {
                                {
                                    setState(273);
                                    match(COMMA);
                                    setState(277);
                                    _errHandler.sync(this);
                                    switch (_input.LA(1)) {
                                        case WORD: {
                                            setState(274);
                                            identifier();
                                        }
                                        break;
                                        case TRUE:
                                        case FALSE: {
                                            setState(275);
                                            booleanValue();
                                        }
                                        break;
                                        case NUMBER:
                                        case MINUS: {
                                            setState(276);
                                            number();
                                        }
                                        break;
                                        default:
                                            throw new NoViableAltException(this);
                                    }
                                }
                            }
                            setState(283);
                            _errHandler.sync(this);
                            _la = _input.LA(1);
                        }
                    }
                }

                setState(286);
                match(CLOSE_BRACKET);
            }
        }
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FunctionBodyContext extends ParserRuleContext {
		public TerminalNode OPEN_FUNC_BRACKET() { return getToken(GrammarParser.OPEN_FUNC_BRACKET, 0); }
		public TerminalNode CLOSE_FUNC_BRACKET() { return getToken(GrammarParser.CLOSE_FUNC_BRACKET, 0); }
		public List<FunctionRowContext> functionRow() {
			return getRuleContexts(FunctionRowContext.class);
		}
		public FunctionRowContext functionRow(int i) {
			return getRuleContext(FunctionRowContext.class,i);
		}
		public FunctionBodyContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_functionBody; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GrammarListener ) ((GrammarListener)listener).enterFunctionBody(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GrammarListener ) ((GrammarListener)listener).exitFunctionBody(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GrammarVisitor ) return ((GrammarVisitor<? extends T>)visitor).visitFunctionBody(this);
			else return visitor.visitChildren(this);
		}
	}

	public final FunctionBodyContext functionBody() throws RecognitionException {
		FunctionBodyContext _localctx = new FunctionBodyContext(_ctx, getState());
		enterRule(_localctx, 58, RULE_functionBody);
		int _la;
		try {
            enterOuterAlt(_localctx, 1);
            {
                setState(288);
                match(OPEN_FUNC_BRACKET);
                setState(292);
                _errHandler.sync(this);
                _la = _input.LA(1);
                while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << WORD) | (1L << QUESTION_MARK) | (1L << CREATE_VAR) | (1L << CREATE_CONST) | (1L << IF_KEYWORD) | (1L << WHILE_KEYWORD) | (1L << UNTIL_KEYWORD) | (1L << DO_KEYWORD) | (1L << RETURN_KEYWORD) | (1L << FOR_KEYWORD))) != 0)) {
                    {
                        {
                            setState(289);
                            functionRow();
                        }
                    }
                    setState(294);
                    _errHandler.sync(this);
                    _la = _input.LA(1);
                }
                setState(295);
                match(CLOSE_FUNC_BRACKET);
            }
        }
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FunctionRowContext extends ParserRuleContext {
		public DefineVariableContext defineVariable() {
			return getRuleContext(DefineVariableContext.class,0);
		}
		public IfFunctionContext ifFunction() {
			return getRuleContext(IfFunctionContext.class,0);
		}
		public WhileFunctionContext whileFunction() {
			return getRuleContext(WhileFunctionContext.class,0);
		}
		public UntilFunctionContext untilFunction() {
			return getRuleContext(UntilFunctionContext.class,0);
		}
		public DoWhileFunctionContext doWhileFunction() {
			return getRuleContext(DoWhileFunctionContext.class,0);
		}
		public DoUntilFunctionContext doUntilFunction() {
			return getRuleContext(DoUntilFunctionContext.class,0);
		}
		public ReturnValueContext returnValue() {
			return getRuleContext(ReturnValueContext.class,0);
		}
		public CallFunctionContext callFunction() {
			return getRuleContext(CallFunctionContext.class,0);
		}
		public TerminalNode SEMICOLON() { return getToken(GrammarParser.SEMICOLON, 0); }
		public ForFunctionContext forFunction() {
			return getRuleContext(ForFunctionContext.class,0);
		}
		public FunctionRowContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_functionRow; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GrammarListener ) ((GrammarListener)listener).enterFunctionRow(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GrammarListener ) ((GrammarListener)listener).exitFunctionRow(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GrammarVisitor ) return ((GrammarVisitor<? extends T>)visitor).visitFunctionRow(this);
			else return visitor.visitChildren(this);
		}
	}

	public final FunctionRowContext functionRow() throws RecognitionException {
		FunctionRowContext _localctx = new FunctionRowContext(_ctx, getState());
		enterRule(_localctx, 60, RULE_functionRow);
		try {
            setState(308);
            _errHandler.sync(this);
            switch (getInterpreter().adaptivePredict(_input, 24, _ctx)) {
                case 1:
                    enterOuterAlt(_localctx, 1);
                {
                    setState(297);
                    defineVariable();
                }
                break;
                case 2:
                    enterOuterAlt(_localctx, 2);
                {
                    setState(298);
                    ifFunction();
                }
                break;
                case 3:
                    enterOuterAlt(_localctx, 3);
                {
                    setState(299);
                    whileFunction();
                }
                break;
                case 4:
                    enterOuterAlt(_localctx, 4);
                {
                    setState(300);
                    untilFunction();
                }
                break;
                case 5:
                    enterOuterAlt(_localctx, 5);
                {
                    setState(301);
                    doWhileFunction();
                }
                break;
                case 6:
                    enterOuterAlt(_localctx, 6);
                {
                    setState(302);
                    doUntilFunction();
                }
                break;
                case 7:
                    enterOuterAlt(_localctx, 7);
                {
                    setState(303);
                    returnValue();
                }
                break;
                case 8:
                    enterOuterAlt(_localctx, 8);
                {
                    {
                        setState(304);
                        callFunction();
                        setState(305);
                        match(SEMICOLON);
                    }
                }
                break;
                case 9:
                    enterOuterAlt(_localctx, 9);
                {
                    setState(307);
                    forFunction();
                }
                break;
            }
        }
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class IfFunctionContext extends ParserRuleContext {
		public TerminalNode IF_KEYWORD() { return getToken(GrammarParser.IF_KEYWORD, 0); }
		public ConditionContext condition() {
			return getRuleContext(ConditionContext.class,0);
		}
		public List<FunctionBodyContext> functionBody() {
			return getRuleContexts(FunctionBodyContext.class);
		}
		public FunctionBodyContext functionBody(int i) {
			return getRuleContext(FunctionBodyContext.class,i);
		}
		public TerminalNode ElSE_KEYWORD() { return getToken(GrammarParser.ElSE_KEYWORD, 0); }
		public TerminalNode QUESTION_MARK() { return getToken(GrammarParser.QUESTION_MARK, 0); }
		public TerminalNode COLON() { return getToken(GrammarParser.COLON, 0); }
		public IfFunctionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_ifFunction; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GrammarListener ) ((GrammarListener)listener).enterIfFunction(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GrammarListener ) ((GrammarListener)listener).exitIfFunction(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GrammarVisitor ) return ((GrammarVisitor<? extends T>)visitor).visitIfFunction(this);
			else return visitor.visitChildren(this);
		}
	}

	public final IfFunctionContext ifFunction() throws RecognitionException {
		IfFunctionContext _localctx = new IfFunctionContext(_ctx, getState());
		enterRule(_localctx, 62, RULE_ifFunction);
		int _la;
		try {
            setState(323);
            _errHandler.sync(this);
            switch (_input.LA(1)) {
                case IF_KEYWORD:
                    enterOuterAlt(_localctx, 1);
                {
                    setState(310);
                    match(IF_KEYWORD);
                    setState(311);
                    condition();
                    setState(312);
                    functionBody();
                    setState(315);
                    _errHandler.sync(this);
                    _la = _input.LA(1);
                    if (_la == ElSE_KEYWORD) {
                        {
                            setState(313);
                            match(ElSE_KEYWORD);
                            setState(314);
                            functionBody();
                        }
                    }

                }
                break;
                case QUESTION_MARK:
                    enterOuterAlt(_localctx, 2);
                {
                    setState(317);
                    match(QUESTION_MARK);
                    setState(318);
                    condition();
                    setState(319);
                    functionBody();
                    setState(320);
                    match(COLON);
                    setState(321);
                    functionBody();
                }
                break;
                default:
                    throw new NoViableAltException(this);
            }
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class WhileFunctionContext extends ParserRuleContext {
		public TerminalNode WHILE_KEYWORD() { return getToken(GrammarParser.WHILE_KEYWORD, 0); }
		public ConditionContext condition() {
			return getRuleContext(ConditionContext.class,0);
		}
		public FunctionBodyContext functionBody() {
			return getRuleContext(FunctionBodyContext.class,0);
		}
		public WhileFunctionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_whileFunction; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GrammarListener ) ((GrammarListener)listener).enterWhileFunction(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GrammarListener ) ((GrammarListener)listener).exitWhileFunction(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GrammarVisitor ) return ((GrammarVisitor<? extends T>)visitor).visitWhileFunction(this);
			else return visitor.visitChildren(this);
		}
	}

	public final WhileFunctionContext whileFunction() throws RecognitionException {
		WhileFunctionContext _localctx = new WhileFunctionContext(_ctx, getState());
		enterRule(_localctx, 64, RULE_whileFunction);
		try {
            enterOuterAlt(_localctx, 1);
            {
                setState(325);
                match(WHILE_KEYWORD);
                setState(326);
                condition();
                setState(327);
                functionBody();
            }
        }
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class DoWhileFunctionContext extends ParserRuleContext {
		public TerminalNode DO_KEYWORD() { return getToken(GrammarParser.DO_KEYWORD, 0); }
		public FunctionBodyContext functionBody() {
			return getRuleContext(FunctionBodyContext.class,0);
		}
		public TerminalNode WHILE_KEYWORD() { return getToken(GrammarParser.WHILE_KEYWORD, 0); }
		public ConditionContext condition() {
			return getRuleContext(ConditionContext.class,0);
		}
		public DoWhileFunctionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_doWhileFunction; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GrammarListener ) ((GrammarListener)listener).enterDoWhileFunction(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GrammarListener ) ((GrammarListener)listener).exitDoWhileFunction(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GrammarVisitor ) return ((GrammarVisitor<? extends T>)visitor).visitDoWhileFunction(this);
			else return visitor.visitChildren(this);
		}
	}

	public final DoWhileFunctionContext doWhileFunction() throws RecognitionException {
		DoWhileFunctionContext _localctx = new DoWhileFunctionContext(_ctx, getState());
		enterRule(_localctx, 66, RULE_doWhileFunction);
		try {
            enterOuterAlt(_localctx, 1);
            {
                setState(329);
                match(DO_KEYWORD);
                setState(330);
                functionBody();
                setState(331);
                match(WHILE_KEYWORD);
                setState(332);
                condition();
            }
        }
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class UntilFunctionContext extends ParserRuleContext {
		public TerminalNode UNTIL_KEYWORD() { return getToken(GrammarParser.UNTIL_KEYWORD, 0); }
		public ConditionContext condition() {
			return getRuleContext(ConditionContext.class,0);
		}
		public FunctionBodyContext functionBody() {
			return getRuleContext(FunctionBodyContext.class,0);
		}
		public UntilFunctionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_untilFunction; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GrammarListener ) ((GrammarListener)listener).enterUntilFunction(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GrammarListener ) ((GrammarListener)listener).exitUntilFunction(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GrammarVisitor ) return ((GrammarVisitor<? extends T>)visitor).visitUntilFunction(this);
			else return visitor.visitChildren(this);
		}
	}

	public final UntilFunctionContext untilFunction() throws RecognitionException {
		UntilFunctionContext _localctx = new UntilFunctionContext(_ctx, getState());
		enterRule(_localctx, 68, RULE_untilFunction);
		try {
            enterOuterAlt(_localctx, 1);
            {
                setState(334);
                match(UNTIL_KEYWORD);
                setState(335);
                condition();
                setState(336);
                functionBody();
            }
        }
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class DoUntilFunctionContext extends ParserRuleContext {
		public TerminalNode DO_KEYWORD() { return getToken(GrammarParser.DO_KEYWORD, 0); }
		public FunctionBodyContext functionBody() {
			return getRuleContext(FunctionBodyContext.class,0);
		}
		public TerminalNode UNTIL_KEYWORD() { return getToken(GrammarParser.UNTIL_KEYWORD, 0); }
		public ConditionContext condition() {
			return getRuleContext(ConditionContext.class,0);
		}
		public DoUntilFunctionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_doUntilFunction; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GrammarListener ) ((GrammarListener)listener).enterDoUntilFunction(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GrammarListener ) ((GrammarListener)listener).exitDoUntilFunction(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GrammarVisitor ) return ((GrammarVisitor<? extends T>)visitor).visitDoUntilFunction(this);
			else return visitor.visitChildren(this);
		}
	}

	public final DoUntilFunctionContext doUntilFunction() throws RecognitionException {
		DoUntilFunctionContext _localctx = new DoUntilFunctionContext(_ctx, getState());
		enterRule(_localctx, 70, RULE_doUntilFunction);
		try {
            enterOuterAlt(_localctx, 1);
            {
                setState(338);
                match(DO_KEYWORD);
                setState(339);
                functionBody();
                setState(340);
                match(UNTIL_KEYWORD);
                setState(341);
                condition();
            }
        }
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ForFunctionContext extends ParserRuleContext {
		public TerminalNode FOR_KEYWORD() { return getToken(GrammarParser.FOR_KEYWORD, 0); }
		public TerminalNode OPEN_BRACKET() { return getToken(GrammarParser.OPEN_BRACKET, 0); }
		public List<IdentifierContext> identifier() {
			return getRuleContexts(IdentifierContext.class);
		}
		public IdentifierContext identifier(int i) {
			return getRuleContext(IdentifierContext.class,i);
		}
		public List<TerminalNode> ASSIGN() { return getTokens(GrammarParser.ASSIGN); }
		public TerminalNode ASSIGN(int i) {
			return getToken(GrammarParser.ASSIGN, i);
		}
		public List<NumeralOperationContext> numeralOperation() {
			return getRuleContexts(NumeralOperationContext.class);
		}
		public NumeralOperationContext numeralOperation(int i) {
			return getRuleContext(NumeralOperationContext.class,i);
		}
		public List<TerminalNode> SEMICOLON() { return getTokens(GrammarParser.SEMICOLON); }
		public TerminalNode SEMICOLON(int i) {
			return getToken(GrammarParser.SEMICOLON, i);
		}
		public LogicalOperationContext logicalOperation() {
			return getRuleContext(LogicalOperationContext.class,0);
		}
		public TerminalNode CLOSE_BRACKET() { return getToken(GrammarParser.CLOSE_BRACKET, 0); }
		public FunctionBodyContext functionBody() {
			return getRuleContext(FunctionBodyContext.class,0);
		}
		public ForFunctionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_forFunction; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GrammarListener ) ((GrammarListener)listener).enterForFunction(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GrammarListener ) ((GrammarListener)listener).exitForFunction(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GrammarVisitor ) return ((GrammarVisitor<? extends T>)visitor).visitForFunction(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ForFunctionContext forFunction() throws RecognitionException {
		ForFunctionContext _localctx = new ForFunctionContext(_ctx, getState());
		enterRule(_localctx, 72, RULE_forFunction);
		try {
            enterOuterAlt(_localctx, 1);
            {
                setState(343);
                match(FOR_KEYWORD);
                setState(344);
                match(OPEN_BRACKET);
                setState(345);
                identifier();
                setState(346);
                match(ASSIGN);
                setState(347);
                numeralOperation(0);
                setState(348);
                match(SEMICOLON);
                setState(349);
                logicalOperation();
                setState(350);
                match(SEMICOLON);
                setState(351);
                identifier();
                setState(352);
                match(ASSIGN);
                setState(353);
                numeralOperation(0);
                setState(354);
                match(CLOSE_BRACKET);
                setState(355);
                functionBody();
            }
        }
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ReturnValueContext extends ParserRuleContext {
		public TerminalNode RETURN_KEYWORD() { return getToken(GrammarParser.RETURN_KEYWORD, 0); }
		public TerminalNode SEMICOLON() { return getToken(GrammarParser.SEMICOLON, 0); }
		public IdentifierContext identifier() {
			return getRuleContext(IdentifierContext.class,0);
		}
		public NumberContext number() {
			return getRuleContext(NumberContext.class,0);
		}
		public BooleanValueContext booleanValue() {
			return getRuleContext(BooleanValueContext.class,0);
		}
		public ReturnValueContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_returnValue; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GrammarListener ) ((GrammarListener)listener).enterReturnValue(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GrammarListener ) ((GrammarListener)listener).exitReturnValue(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GrammarVisitor ) return ((GrammarVisitor<? extends T>)visitor).visitReturnValue(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ReturnValueContext returnValue() throws RecognitionException {
		ReturnValueContext _localctx = new ReturnValueContext(_ctx, getState());
		enterRule(_localctx, 74, RULE_returnValue);
		try {
            enterOuterAlt(_localctx, 1);
            {
                setState(357);
                match(RETURN_KEYWORD);
                setState(361);
                _errHandler.sync(this);
                switch (_input.LA(1)) {
                    case WORD: {
                        setState(358);
                        identifier();
                    }
                    break;
                    case NUMBER:
                    case MINUS: {
                        setState(359);
                        number();
                    }
                    break;
                    case TRUE:
                    case FALSE: {
                        setState(360);
                        booleanValue();
                    }
                    break;
                    case SEMICOLON:
                        break;
                    default:
                        break;
                }
                setState(363);
                match(SEMICOLON);
            }
        }
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public boolean sempred(RuleContext _localctx, int ruleIndex, int predIndex) {
		switch (ruleIndex) {
		case 14:
			return numeralOperation_sempred((NumeralOperationContext)_localctx, predIndex);
            case 15:
                return numeralOperationInner_sempred((NumeralOperationInnerContext) _localctx, predIndex);
        }
        return true;
    }

    private boolean numeralOperation_sempred(NumeralOperationContext _localctx, int predIndex) {
        switch (predIndex) {
            case 0:
                return precpred(_ctx, 2);
        }
        return true;
    }

    private boolean numeralOperationInner_sempred(NumeralOperationInnerContext _localctx, int predIndex) {
        switch (predIndex) {
            case 1:
                return precpred(_ctx, 2);
        }
        return true;
    }

    public static final String _serializedATN =
            "\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\3+\u0170\4\2\t\2\4" +
                    "\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t" +
                    "\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22" +
                    "\4\23\t\23\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30\4\31\t\31" +
                    "\4\32\t\32\4\33\t\33\4\34\t\34\4\35\t\35\4\36\t\36\4\37\t\37\4 \t \4!" +
                    "\t!\4\"\t\"\4#\t#\4$\t$\4%\t%\4&\t&\4\'\t\'\3\2\3\2\3\2\3\2\3\2\3\2\3" +
                    "\2\3\2\3\2\3\2\3\2\7\2Z\n\2\f\2\16\2]\13\2\3\3\3\3\3\4\5\4b\n\4\3\4\3" +
                    "\4\7\4f\n\4\f\4\16\4i\13\4\3\5\3\5\7\5m\n\5\f\5\16\5p\13\5\3\6\3\6\3\7" +
                    "\3\7\5\7v\n\7\3\b\3\b\3\t\3\t\3\n\3\n\3\13\3\13\3\f\3\f\3\r\3\r\3\r\3" +
                    "\r\7\r\u0086\n\r\f\r\16\r\u0089\13\r\3\16\5\16\u008c\n\16\3\16\3\16\3" +
                    "\17\3\17\3\17\3\17\3\17\3\17\5\17\u0096\n\17\3\20\3\20\3\20\3\20\3\20" +
                    "\3\20\3\20\7\20\u009f\n\20\f\20\16\20\u00a2\13\20\3\21\3\21\3\21\3\21" +
                    "\3\21\3\21\3\21\7\21\u00ab\n\21\f\21\16\21\u00ae\13\21\3\22\3\22\3\22" +
                    "\5\22\u00b3\n\22\3\23\3\23\3\23\3\23\3\24\3\24\3\24\3\24\5\24\u00bd\n" +
                    "\24\3\25\3\25\3\25\3\25\3\25\3\25\3\26\3\26\3\26\3\26\7\26\u00c9\n\26" +
                    "\f\26\16\26\u00cc\13\26\5\26\u00ce\n\26\3\26\3\26\3\27\3\27\3\27\3\30" +
                    "\3\30\3\30\3\30\3\31\3\31\3\31\3\31\3\31\3\31\3\31\7\31\u00e0\n\31\f\31" +
                    "\16\31\u00e3\13\31\3\31\3\31\3\31\3\31\3\31\3\31\3\31\3\31\3\31\3\31\5" +
                    "\31\u00ef\n\31\3\32\3\32\3\32\3\32\6\32\u00f5\n\32\r\32\16\32\u00f6\3" +
                    "\32\3\32\3\33\3\33\3\33\3\33\6\33\u00ff\n\33\r\33\16\33\u0100\3\33\3\33" +
                    "\3\34\3\34\3\34\3\34\3\34\3\34\3\35\3\35\3\35\3\36\3\36\3\36\3\36\5\36" +
                    "\u0112\n\36\3\36\3\36\3\36\3\36\5\36\u0118\n\36\7\36\u011a\n\36\f\36\16" +
                    "\36\u011d\13\36\5\36\u011f\n\36\3\36\3\36\3\37\3\37\7\37\u0125\n\37\f" +
                    "\37\16\37\u0128\13\37\3\37\3\37\3 \3 \3 \3 \3 \3 \3 \3 \3 \3 \3 \5 \u0137" +
                    "\n \3!\3!\3!\3!\3!\5!\u013e\n!\3!\3!\3!\3!\3!\3!\5!\u0146\n!\3\"\3\"\3" +
                    "\"\3\"\3#\3#\3#\3#\3#\3$\3$\3$\3$\3%\3%\3%\3%\3%\3&\3&\3&\3&\3&\3&\3&" +
                    "\3&\3&\3&\3&\3&\3&\3&\3\'\3\'\3\'\3\'\5\'\u016c\n\'\3\'\3\'\3\'\2\4\36" +
                    " (\2\4\6\b\n\f\16\20\22\24\26\30\32\34\36 \"$&(*,.\60\62\64\668:<>@BD" +
                    "FHJL\2\t\3\2\6\7\3\2\21\22\3\2\24\25\3\2\27\30\3\2\31\32\3\2\33 \3\2!" +
                    "\"\2\u017b\2[\3\2\2\2\4^\3\2\2\2\6a\3\2\2\2\bj\3\2\2\2\nq\3\2\2\2\fu\3" +
                    "\2\2\2\16w\3\2\2\2\20y\3\2\2\2\22{\3\2\2\2\24}\3\2\2\2\26\177\3\2\2\2" +
                    "\30\u0081\3\2\2\2\32\u008b\3\2\2\2\34\u0095\3\2\2\2\36\u0097\3\2\2\2 " +
                    "\u00a3\3\2\2\2\"\u00b2\3\2\2\2$\u00b4\3\2\2\2&\u00bc\3\2\2\2(\u00be\3" +
                    "\2\2\2*\u00c4\3\2\2\2,\u00d1\3\2\2\2.\u00d4\3\2\2\2\60\u00ee\3\2\2\2\62" +
                    "\u00f0\3\2\2\2\64\u00fa\3\2\2\2\66\u0104\3\2\2\28\u010a\3\2\2\2:\u010d" +
                    "\3\2\2\2<\u0122\3\2\2\2>\u0136\3\2\2\2@\u0145\3\2\2\2B\u0147\3\2\2\2D" +
                    "\u014b\3\2\2\2F\u0150\3\2\2\2H\u0154\3\2\2\2J\u0159\3\2\2\2L\u0167\3\2" +
                    "\2\2NZ\5\60\31\2OZ\5\66\34\2PQ\58\35\2QR\7\r\2\2RZ\3\2\2\2SZ\5B\"\2TZ" +
                    "\5D#\2UZ\5H%\2VZ\5@!\2WZ\5F$\2XZ\5J&\2YN\3\2\2\2YO\3\2\2\2YP\3\2\2\2Y" +
                    "S\3\2\2\2YT\3\2\2\2YU\3\2\2\2YV\3\2\2\2YW\3\2\2\2YX\3\2\2\2Z]\3\2\2\2" +
                    "[Y\3\2\2\2[\\\3\2\2\2\\\3\3\2\2\2][\3\2\2\2^_\t\2\2\2_\5\3\2\2\2`b\7\30" +
                    "\2\2a`\3\2\2\2ab\3\2\2\2bc\3\2\2\2cg\7\4\2\2df\7\4\2\2ed\3\2\2\2fi\3\2" +
                    "\2\2ge\3\2\2\2gh\3\2\2\2h\7\3\2\2\2ig\3\2\2\2jn\7\3\2\2km\7\3\2\2lk\3" +
                    "\2\2\2mp\3\2\2\2nl\3\2\2\2no\3\2\2\2o\t\3\2\2\2pn\3\2\2\2qr\t\3\2\2r\13" +
                    "\3\2\2\2sv\5\n\6\2tv\7\23\2\2us\3\2\2\2ut\3\2\2\2v\r\3\2\2\2wx\t\4\2\2" +
                    "x\17\3\2\2\2yz\t\5\2\2z\21\3\2\2\2{|\t\6\2\2|\23\3\2\2\2}~\t\7\2\2~\25" +
                    "\3\2\2\2\177\u0080\t\b\2\2\u0080\27\3\2\2\2\u0081\u0087\5\32\16\2\u0082" +
                    "\u0083\5\26\f\2\u0083\u0084\5\32\16\2\u0084\u0086\3\2\2\2\u0085\u0082" +
                    "\3\2\2\2\u0086\u0089\3\2\2\2\u0087\u0085\3\2\2\2\u0087\u0088\3\2\2\2\u0088" +
                    "\31\3\2\2\2\u0089\u0087\3\2\2\2\u008a\u008c\7#\2\2\u008b\u008a\3\2\2\2" +
                    "\u008b\u008c\3\2\2\2\u008c\u008d\3\2\2\2\u008d\u008e\5\34\17\2\u008e\33" +
                    "\3\2\2\2\u008f\u0096\5\b\5\2\u0090\u0096\5\4\3\2\u0091\u0092\5\36\20\2" +
                    "\u0092\u0093\5\24\13\2\u0093\u0094\5\36\20\2\u0094\u0096\3\2\2\2\u0095" +
                    "\u008f\3\2\2\2\u0095\u0090\3\2\2\2\u0095\u0091\3\2\2\2\u0096\35\3\2\2" +
                    "\2\u0097\u0098\b\20\1\2\u0098\u0099\5 \21\2\u0099\u00a0\3\2\2\2\u009a" +
                    "\u009b\f\4\2\2\u009b\u009c\5\20\t\2\u009c\u009d\5 \21\2\u009d\u009f\3" +
                    "\2\2\2\u009e\u009a\3\2\2\2\u009f\u00a2\3\2\2\2\u00a0\u009e\3\2\2\2\u00a0" +
                    "\u00a1\3\2\2\2\u00a1\37\3\2\2\2\u00a2\u00a0\3\2\2\2\u00a3\u00a4\b\21\1" +
                    "\2\u00a4\u00a5\5\"\22\2\u00a5\u00ac\3\2\2\2\u00a6\u00a7\f\4\2\2\u00a7" +
                    "\u00a8\5\22\n\2\u00a8\u00a9\5\"\22\2\u00a9\u00ab\3\2\2\2\u00aa\u00a6\3" +
                    "\2\2\2\u00ab\u00ae\3\2\2\2\u00ac\u00aa\3\2\2\2\u00ac\u00ad\3\2\2\2\u00ad" +
                    "!\3\2\2\2\u00ae\u00ac\3\2\2\2\u00af\u00b3\5\b\5\2\u00b0\u00b3\5\6\4\2" +
                    "\u00b1\u00b3\5$\23\2\u00b2\u00af\3\2\2\2\u00b2\u00b0\3\2\2\2\u00b2\u00b1" +
                    "\3\2\2\2\u00b3#\3\2\2\2\u00b4\u00b5\7\b\2\2\u00b5\u00b6\5\36\20\2\u00b6" +
                    "\u00b7\7\t\2\2\u00b7%\3\2\2\2\u00b8\u00bd\5\36\20\2\u00b9\u00bd\5\30\r" +
                    "\2\u00ba\u00bd\58\35\2\u00bb\u00bd\5(\25\2\u00bc\u00b8\3\2\2\2\u00bc\u00b9" +
                    "\3\2\2\2\u00bc\u00ba\3\2\2\2\u00bc\u00bb\3\2\2\2\u00bd\'\3\2\2\2\u00be" +
                    "\u00bf\5.\30\2\u00bf\u00c0\7\17\2\2\u00c0\u00c1\5&\24\2\u00c1\u00c2\7" +
                    "\20\2\2\u00c2\u00c3\5&\24\2\u00c3)\3\2\2\2\u00c4\u00cd\7\b\2\2\u00c5\u00ca" +
                    "\5,\27\2\u00c6\u00c7\7\f\2\2\u00c7\u00c9\5,\27\2\u00c8\u00c6\3\2\2\2\u00c9" +
                    "\u00cc\3\2\2\2\u00ca\u00c8\3\2\2\2\u00ca\u00cb\3\2\2\2\u00cb\u00ce\3\2" +
                    "\2\2\u00cc\u00ca\3\2\2\2\u00cd\u00c5\3\2\2\2\u00cd\u00ce\3\2\2\2\u00ce" +
                    "\u00cf\3\2\2\2\u00cf\u00d0\7\t\2\2\u00d0+\3\2\2\2\u00d1\u00d2\5\n\6\2" +
                    "\u00d2\u00d3\5\b\5\2\u00d3-\3\2\2\2\u00d4\u00d5\7\b\2\2\u00d5\u00d6\5" +
                    "\30\r\2\u00d6\u00d7\7\t\2\2\u00d7/\3\2\2\2\u00d8\u00d9\5\16\b\2\u00d9" +
                    "\u00da\5\n\6\2\u00da\u00db\5\b\5\2\u00db\u00e1\7\26\2\2\u00dc\u00dd\5" +
                    "\b\5\2\u00dd\u00de\7\26\2\2\u00de\u00e0\3\2\2\2\u00df\u00dc\3\2\2\2\u00e0" +
                    "\u00e3\3\2\2\2\u00e1\u00df\3\2\2\2\u00e1\u00e2\3\2\2\2\u00e2\u00e4\3\2" +
                    "\2\2\u00e3\u00e1\3\2\2\2\u00e4\u00e5\5&\24\2\u00e5\u00e6\7\r\2\2\u00e6" +
                    "\u00ef\3\2\2\2\u00e7\u00e8\5\16\b\2\u00e8\u00e9\5\n\6\2\u00e9\u00ea\5" +
                    "\62\32\2\u00ea\u00eb\7\26\2\2\u00eb\u00ec\5\64\33\2\u00ec\u00ed\7\r\2" +
                    "\2\u00ed\u00ef\3\2\2\2\u00ee\u00d8\3\2\2\2\u00ee\u00e7\3\2\2\2\u00ef\61" +
                    "\3\2\2\2\u00f0\u00f1\7\n\2\2\u00f1\u00f4\5\b\5\2\u00f2\u00f3\7\f\2\2\u00f3" +
                    "\u00f5\5\b\5\2\u00f4\u00f2\3\2\2\2\u00f5\u00f6\3\2\2\2\u00f6\u00f4\3\2" +
                    "\2\2\u00f6\u00f7\3\2\2\2\u00f7\u00f8\3\2\2\2\u00f8\u00f9\7\13\2\2\u00f9" +
                    "\63\3\2\2\2\u00fa\u00fb\7\n\2\2\u00fb\u00fe\5&\24\2\u00fc\u00fd\7\f\2" +
                    "\2\u00fd\u00ff\5&\24\2\u00fe\u00fc\3\2\2\2\u00ff\u0100\3\2\2\2\u0100\u00fe" +
                    "\3\2\2\2\u0100\u0101\3\2\2\2\u0101\u0102\3\2\2\2\u0102\u0103\7\13\2\2" +
                    "\u0103\65\3\2\2\2\u0104\u0105\7$\2\2\u0105\u0106\5\f\7\2\u0106\u0107\5" +
                    "\b\5\2\u0107\u0108\5*\26\2\u0108\u0109\5<\37\2\u0109\67\3\2\2\2\u010a" +
                    "\u010b\5\b\5\2\u010b\u010c\5:\36\2\u010c9\3\2\2\2\u010d\u011e\7\b\2\2" +
                    "\u010e\u0112\5\b\5\2\u010f\u0112\5\4\3\2\u0110\u0112\5\6\4\2\u0111\u010e" +
                    "\3\2\2\2\u0111\u010f\3\2\2\2\u0111\u0110\3\2\2\2\u0112\u011b\3\2\2\2\u0113" +
                    "\u0117\7\f\2\2\u0114\u0118\5\b\5\2\u0115\u0118\5\4\3\2\u0116\u0118\5\6" +
                    "\4\2\u0117\u0114\3\2\2\2\u0117\u0115\3\2\2\2\u0117\u0116\3\2\2\2\u0118" +
                    "\u011a\3\2\2\2\u0119\u0113\3\2\2\2\u011a\u011d\3\2\2\2\u011b\u0119\3\2" +
                    "\2\2\u011b\u011c\3\2\2\2\u011c\u011f\3\2\2\2\u011d\u011b\3\2\2\2\u011e" +
                    "\u0111\3\2\2\2\u011e\u011f\3\2\2\2\u011f\u0120\3\2\2\2\u0120\u0121\7\t" +
                    "\2\2\u0121;\3\2\2\2\u0122\u0126\7\n\2\2\u0123\u0125\5> \2\u0124\u0123" +
                    "\3\2\2\2\u0125\u0128\3\2\2\2\u0126\u0124\3\2\2\2\u0126\u0127\3\2\2\2\u0127" +
                    "\u0129\3\2\2\2\u0128\u0126\3\2\2\2\u0129\u012a\7\13\2\2\u012a=\3\2\2\2" +
                    "\u012b\u0137\5\60\31\2\u012c\u0137\5@!\2\u012d\u0137\5B\"\2\u012e\u0137" +
                    "\5F$\2\u012f\u0137\5D#\2\u0130\u0137\5H%\2\u0131\u0137\5L\'\2\u0132\u0133" +
                    "\58\35\2\u0133\u0134\7\r\2\2\u0134\u0137\3\2\2\2\u0135\u0137\5J&\2\u0136" +
                    "\u012b\3\2\2\2\u0136\u012c\3\2\2\2\u0136\u012d\3\2\2\2\u0136\u012e\3\2" +
                    "\2\2\u0136\u012f\3\2\2\2\u0136\u0130\3\2\2\2\u0136\u0131\3\2\2\2\u0136" +
                    "\u0132\3\2\2\2\u0136\u0135\3\2\2\2\u0137?\3\2\2\2\u0138\u0139\7%\2\2\u0139" +
                    "\u013a\5.\30\2\u013a\u013d\5<\37\2\u013b\u013c\7*\2\2\u013c\u013e\5<\37" +
                    "\2\u013d\u013b\3\2\2\2\u013d\u013e\3\2\2\2\u013e\u0146\3\2\2\2\u013f\u0140" +
                    "\7\17\2\2\u0140\u0141\5.\30\2\u0141\u0142\5<\37\2\u0142\u0143\7\20\2\2" +
                    "\u0143\u0144\5<\37\2\u0144\u0146\3\2\2\2\u0145\u0138\3\2\2\2\u0145\u013f" +
                    "\3\2\2\2\u0146A\3\2\2\2\u0147\u0148\7&\2\2\u0148\u0149\5.\30\2\u0149\u014a" +
                    "\5<\37\2\u014aC\3\2\2\2\u014b\u014c\7(\2\2\u014c\u014d\5<\37\2\u014d\u014e" +
                    "\7&\2\2\u014e\u014f\5.\30\2\u014fE\3\2\2\2\u0150\u0151\7\'\2\2\u0151\u0152" +
                    "\5.\30\2\u0152\u0153\5<\37\2\u0153G\3\2\2\2\u0154\u0155\7(\2\2\u0155\u0156" +
                    "\5<\37\2\u0156\u0157\7\'\2\2\u0157\u0158\5.\30\2\u0158I\3\2\2\2\u0159" +
                    "\u015a\7+\2\2\u015a\u015b\7\b\2\2\u015b\u015c\5\b\5\2\u015c\u015d\7\26" +
                    "\2\2\u015d\u015e\5\36\20\2\u015e\u015f\7\r\2\2\u015f\u0160\5\30\r\2\u0160" +
                    "\u0161\7\r\2\2\u0161\u0162\5\b\5\2\u0162\u0163\7\26\2\2\u0163\u0164\5" +
                    "\36\20\2\u0164\u0165\7\t\2\2\u0165\u0166\5<\37\2\u0166K\3\2\2\2\u0167" +
                    "\u016b\7)\2\2\u0168\u016c\5\b\5\2\u0169\u016c\5\6\4\2\u016a\u016c\5\4" +
                    "\3\2\u016b\u0168\3\2\2\2\u016b\u0169\3\2\2\2\u016b\u016a\3\2\2\2\u016b" +
                    "\u016c\3\2\2\2\u016c\u016d\3\2\2\2\u016d\u016e\7\r\2\2\u016eM\3\2\2\2" +
                    "\36Y[agnu\u0087\u008b\u0095\u00a0\u00ac\u00b2\u00bc\u00ca\u00cd\u00e1" +
                    "\u00ee\u00f6\u0100\u0111\u0117\u011b\u011e\u0126\u0136\u013d\u0145\u016b";
    public static final ATN _ATN =
            new ATNDeserializer().deserialize(_serializedATN.toCharArray());

    static {
        _decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
        for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
            _decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
        }
    }
}