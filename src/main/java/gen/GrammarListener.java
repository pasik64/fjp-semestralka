// Generated from C:/Users/HP/IdeaProjects/fjp-semestralka/src/main\Grammar.g4 by ANTLR 4.8
package gen;
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link GrammarParser}.
 */
public interface GrammarListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link GrammarParser#entryPoint}.
	 * @param ctx the parse tree
	 */
	void enterEntryPoint(GrammarParser.EntryPointContext ctx);
	/**
	 * Exit a parse tree produced by {@link GrammarParser#entryPoint}.
	 * @param ctx the parse tree
	 */
	void exitEntryPoint(GrammarParser.EntryPointContext ctx);
	/**
	 * Enter a parse tree produced by {@link GrammarParser#booleanValue}.
	 * @param ctx the parse tree
	 */
	void enterBooleanValue(GrammarParser.BooleanValueContext ctx);
	/**
	 * Exit a parse tree produced by {@link GrammarParser#booleanValue}.
	 * @param ctx the parse tree
	 */
	void exitBooleanValue(GrammarParser.BooleanValueContext ctx);
	/**
	 * Enter a parse tree produced by {@link GrammarParser#number}.
	 * @param ctx the parse tree
	 */
	void enterNumber(GrammarParser.NumberContext ctx);
	/**
	 * Exit a parse tree produced by {@link GrammarParser#number}.
	 * @param ctx the parse tree
	 */
	void exitNumber(GrammarParser.NumberContext ctx);
	/**
	 * Enter a parse tree produced by {@link GrammarParser#identifier}.
	 * @param ctx the parse tree
	 */
	void enterIdentifier(GrammarParser.IdentifierContext ctx);
	/**
	 * Exit a parse tree produced by {@link GrammarParser#identifier}.
	 * @param ctx the parse tree
	 */
	void exitIdentifier(GrammarParser.IdentifierContext ctx);
	/**
	 * Enter a parse tree produced by {@link GrammarParser#dateType}.
	 * @param ctx the parse tree
	 */
	void enterDateType(GrammarParser.DateTypeContext ctx);
	/**
	 * Exit a parse tree produced by {@link GrammarParser#dateType}.
	 * @param ctx the parse tree
	 */
	void exitDateType(GrammarParser.DateTypeContext ctx);
	/**
	 * Enter a parse tree produced by {@link GrammarParser#dateTypeFce}.
	 * @param ctx the parse tree
	 */
	void enterDateTypeFce(GrammarParser.DateTypeFceContext ctx);
	/**
	 * Exit a parse tree produced by {@link GrammarParser#dateTypeFce}.
	 * @param ctx the parse tree
	 */
	void exitDateTypeFce(GrammarParser.DateTypeFceContext ctx);
	/**
	 * Enter a parse tree produced by {@link GrammarParser#create}.
	 * @param ctx the parse tree
	 */
	void enterCreate(GrammarParser.CreateContext ctx);
	/**
	 * Exit a parse tree produced by {@link GrammarParser#create}.
	 * @param ctx the parse tree
	 */
	void exitCreate(GrammarParser.CreateContext ctx);
	/**
	 * Enter a parse tree produced by {@link GrammarParser#addOperator}.
	 * @param ctx the parse tree
	 */
	void enterAddOperator(GrammarParser.AddOperatorContext ctx);
	/**
	 * Exit a parse tree produced by {@link GrammarParser#addOperator}.
	 * @param ctx the parse tree
	 */
	void exitAddOperator(GrammarParser.AddOperatorContext ctx);
	/**
	 * Enter a parse tree produced by {@link GrammarParser#multiplyOperator}.
	 * @param ctx the parse tree
	 */
	void enterMultiplyOperator(GrammarParser.MultiplyOperatorContext ctx);
	/**
	 * Exit a parse tree produced by {@link GrammarParser#multiplyOperator}.
	 * @param ctx the parse tree
	 */
	void exitMultiplyOperator(GrammarParser.MultiplyOperatorContext ctx);
	/**
	 * Enter a parse tree produced by {@link GrammarParser#logicalOperator}.
	 * @param ctx the parse tree
	 */
	void enterLogicalOperator(GrammarParser.LogicalOperatorContext ctx);
	/**
	 * Exit a parse tree produced by {@link GrammarParser#logicalOperator}.
	 * @param ctx the parse tree
	 */
	void exitLogicalOperator(GrammarParser.LogicalOperatorContext ctx);
	/**
	 * Enter a parse tree produced by {@link GrammarParser#logicalOperatorBinary}.
	 * @param ctx the parse tree
	 */
	void enterLogicalOperatorBinary(GrammarParser.LogicalOperatorBinaryContext ctx);
	/**
	 * Exit a parse tree produced by {@link GrammarParser#logicalOperatorBinary}.
	 * @param ctx the parse tree
	 */
	void exitLogicalOperatorBinary(GrammarParser.LogicalOperatorBinaryContext ctx);
	/**
	 * Enter a parse tree produced by {@link GrammarParser#logicalOperation}.
	 * @param ctx the parse tree
	 */
	void enterLogicalOperation(GrammarParser.LogicalOperationContext ctx);
	/**
	 * Exit a parse tree produced by {@link GrammarParser#logicalOperation}.
	 * @param ctx the parse tree
	 */
	void exitLogicalOperation(GrammarParser.LogicalOperationContext ctx);
	/**
	 * Enter a parse tree produced by {@link GrammarParser#negateLogicalOperation}.
	 * @param ctx the parse tree
	 */
	void enterNegateLogicalOperation(GrammarParser.NegateLogicalOperationContext ctx);
	/**
	 * Exit a parse tree produced by {@link GrammarParser#negateLogicalOperation}.
	 * @param ctx the parse tree
	 */
	void exitNegateLogicalOperation(GrammarParser.NegateLogicalOperationContext ctx);
	/**
	 * Enter a parse tree produced by {@link GrammarParser#logicalOperationInner}.
	 * @param ctx the parse tree
	 */
	void enterLogicalOperationInner(GrammarParser.LogicalOperationInnerContext ctx);
	/**
	 * Exit a parse tree produced by {@link GrammarParser#logicalOperationInner}.
	 * @param ctx the parse tree
	 */
	void exitLogicalOperationInner(GrammarParser.LogicalOperationInnerContext ctx);
	/**
	 * Enter a parse tree produced by {@link GrammarParser#numeralOperation}.
	 * @param ctx the parse tree
	 */
	void enterNumeralOperation(GrammarParser.NumeralOperationContext ctx);
	/**
	 * Exit a parse tree produced by {@link GrammarParser#numeralOperation}.
	 * @param ctx the parse tree
	 */
	void exitNumeralOperation(GrammarParser.NumeralOperationContext ctx);
	/**
	 * Enter a parse tree produced by {@link GrammarParser#numeralOperationInner}.
	 * @param ctx the parse tree
	 */
	void enterNumeralOperationInner(GrammarParser.NumeralOperationInnerContext ctx);
	/**
	 * Exit a parse tree produced by {@link GrammarParser#numeralOperationInner}.
	 * @param ctx the parse tree
	 */
	void exitNumeralOperationInner(GrammarParser.NumeralOperationInnerContext ctx);
	/**
	 * Enter a parse tree produced by {@link GrammarParser#numeralOperationFinal}.
	 * @param ctx the parse tree
	 */
	void enterNumeralOperationFinal(GrammarParser.NumeralOperationFinalContext ctx);
	/**
	 * Exit a parse tree produced by {@link GrammarParser#numeralOperationFinal}.
	 * @param ctx the parse tree
	 */
	void exitNumeralOperationFinal(GrammarParser.NumeralOperationFinalContext ctx);
	/**
	 * Enter a parse tree produced by {@link GrammarParser#bracketsOperation}.
	 * @param ctx the parse tree
	 */
	void enterBracketsOperation(GrammarParser.BracketsOperationContext ctx);
	/**
	 * Exit a parse tree produced by {@link GrammarParser#bracketsOperation}.
	 * @param ctx the parse tree
	 */
	void exitBracketsOperation(GrammarParser.BracketsOperationContext ctx);
	/**
	 * Enter a parse tree produced by {@link GrammarParser#operation}.
	 * @param ctx the parse tree
	 */
	void enterOperation(GrammarParser.OperationContext ctx);
	/**
	 * Exit a parse tree produced by {@link GrammarParser#operation}.
	 * @param ctx the parse tree
	 */
	void exitOperation(GrammarParser.OperationContext ctx);
	/**
	 * Enter a parse tree produced by {@link GrammarParser#ternaryOperation}.
	 * @param ctx the parse tree
	 */
	void enterTernaryOperation(GrammarParser.TernaryOperationContext ctx);
	/**
	 * Exit a parse tree produced by {@link GrammarParser#ternaryOperation}.
	 * @param ctx the parse tree
	 */
	void exitTernaryOperation(GrammarParser.TernaryOperationContext ctx);
	/**
	 * Enter a parse tree produced by {@link GrammarParser#parameters}.
	 * @param ctx the parse tree
	 */
	void enterParameters(GrammarParser.ParametersContext ctx);
	/**
	 * Exit a parse tree produced by {@link GrammarParser#parameters}.
	 * @param ctx the parse tree
	 */
	void exitParameters(GrammarParser.ParametersContext ctx);
	/**
	 * Enter a parse tree produced by {@link GrammarParser#parameter}.
	 * @param ctx the parse tree
	 */
	void enterParameter(GrammarParser.ParameterContext ctx);
	/**
	 * Exit a parse tree produced by {@link GrammarParser#parameter}.
	 * @param ctx the parse tree
	 */
	void exitParameter(GrammarParser.ParameterContext ctx);
	/**
	 * Enter a parse tree produced by {@link GrammarParser#condition}.
	 * @param ctx the parse tree
	 */
	void enterCondition(GrammarParser.ConditionContext ctx);
	/**
	 * Exit a parse tree produced by {@link GrammarParser#condition}.
	 * @param ctx the parse tree
	 */
	void exitCondition(GrammarParser.ConditionContext ctx);
	/**
	 * Enter a parse tree produced by {@link GrammarParser#defineVariable}.
	 * @param ctx the parse tree
	 */
	void enterDefineVariable(GrammarParser.DefineVariableContext ctx);
	/**
	 * Exit a parse tree produced by {@link GrammarParser#defineVariable}.
	 * @param ctx the parse tree
	 */
	void exitDefineVariable(GrammarParser.DefineVariableContext ctx);
	/**
	 * Enter a parse tree produced by {@link GrammarParser#multipleIdentifiers}.
	 * @param ctx the parse tree
	 */
	void enterMultipleIdentifiers(GrammarParser.MultipleIdentifiersContext ctx);
	/**
	 * Exit a parse tree produced by {@link GrammarParser#multipleIdentifiers}.
	 * @param ctx the parse tree
	 */
	void exitMultipleIdentifiers(GrammarParser.MultipleIdentifiersContext ctx);
	/**
	 * Enter a parse tree produced by {@link GrammarParser#multipleOperations}.
	 * @param ctx the parse tree
	 */
	void enterMultipleOperations(GrammarParser.MultipleOperationsContext ctx);
	/**
	 * Exit a parse tree produced by {@link GrammarParser#multipleOperations}.
	 * @param ctx the parse tree
	 */
	void exitMultipleOperations(GrammarParser.MultipleOperationsContext ctx);
	/**
	 * Enter a parse tree produced by {@link GrammarParser#defineFunction}.
	 * @param ctx the parse tree
	 */
	void enterDefineFunction(GrammarParser.DefineFunctionContext ctx);
	/**
	 * Exit a parse tree produced by {@link GrammarParser#defineFunction}.
	 * @param ctx the parse tree
	 */
	void exitDefineFunction(GrammarParser.DefineFunctionContext ctx);
	/**
	 * Enter a parse tree produced by {@link GrammarParser#callFunction}.
	 * @param ctx the parse tree
	 */
	void enterCallFunction(GrammarParser.CallFunctionContext ctx);
	/**
	 * Exit a parse tree produced by {@link GrammarParser#callFunction}.
	 * @param ctx the parse tree
	 */
	void exitCallFunction(GrammarParser.CallFunctionContext ctx);
	/**
	 * Enter a parse tree produced by {@link GrammarParser#callFunctionParameters}.
	 * @param ctx the parse tree
	 */
	void enterCallFunctionParameters(GrammarParser.CallFunctionParametersContext ctx);
	/**
	 * Exit a parse tree produced by {@link GrammarParser#callFunctionParameters}.
	 * @param ctx the parse tree
	 */
	void exitCallFunctionParameters(GrammarParser.CallFunctionParametersContext ctx);
	/**
	 * Enter a parse tree produced by {@link GrammarParser#functionBody}.
	 * @param ctx the parse tree
	 */
	void enterFunctionBody(GrammarParser.FunctionBodyContext ctx);
	/**
	 * Exit a parse tree produced by {@link GrammarParser#functionBody}.
	 * @param ctx the parse tree
	 */
	void exitFunctionBody(GrammarParser.FunctionBodyContext ctx);
	/**
	 * Enter a parse tree produced by {@link GrammarParser#functionRow}.
	 * @param ctx the parse tree
	 */
	void enterFunctionRow(GrammarParser.FunctionRowContext ctx);
	/**
	 * Exit a parse tree produced by {@link GrammarParser#functionRow}.
	 * @param ctx the parse tree
	 */
	void exitFunctionRow(GrammarParser.FunctionRowContext ctx);
	/**
	 * Enter a parse tree produced by {@link GrammarParser#ifFunction}.
	 * @param ctx the parse tree
	 */
	void enterIfFunction(GrammarParser.IfFunctionContext ctx);
	/**
	 * Exit a parse tree produced by {@link GrammarParser#ifFunction}.
	 * @param ctx the parse tree
	 */
	void exitIfFunction(GrammarParser.IfFunctionContext ctx);
	/**
	 * Enter a parse tree produced by {@link GrammarParser#whileFunction}.
	 * @param ctx the parse tree
	 */
	void enterWhileFunction(GrammarParser.WhileFunctionContext ctx);
	/**
	 * Exit a parse tree produced by {@link GrammarParser#whileFunction}.
	 * @param ctx the parse tree
	 */
	void exitWhileFunction(GrammarParser.WhileFunctionContext ctx);
	/**
	 * Enter a parse tree produced by {@link GrammarParser#doWhileFunction}.
	 * @param ctx the parse tree
	 */
	void enterDoWhileFunction(GrammarParser.DoWhileFunctionContext ctx);
	/**
	 * Exit a parse tree produced by {@link GrammarParser#doWhileFunction}.
	 * @param ctx the parse tree
	 */
	void exitDoWhileFunction(GrammarParser.DoWhileFunctionContext ctx);
	/**
	 * Enter a parse tree produced by {@link GrammarParser#untilFunction}.
	 * @param ctx the parse tree
	 */
	void enterUntilFunction(GrammarParser.UntilFunctionContext ctx);
	/**
	 * Exit a parse tree produced by {@link GrammarParser#untilFunction}.
	 * @param ctx the parse tree
	 */
	void exitUntilFunction(GrammarParser.UntilFunctionContext ctx);
	/**
	 * Enter a parse tree produced by {@link GrammarParser#doUntilFunction}.
	 * @param ctx the parse tree
	 */
	void enterDoUntilFunction(GrammarParser.DoUntilFunctionContext ctx);
	/**
	 * Exit a parse tree produced by {@link GrammarParser#doUntilFunction}.
	 * @param ctx the parse tree
	 */
	void exitDoUntilFunction(GrammarParser.DoUntilFunctionContext ctx);
	/**
	 * Enter a parse tree produced by {@link GrammarParser#forFunction}.
	 * @param ctx the parse tree
	 */
	void enterForFunction(GrammarParser.ForFunctionContext ctx);
	/**
	 * Exit a parse tree produced by {@link GrammarParser#forFunction}.
	 * @param ctx the parse tree
	 */
	void exitForFunction(GrammarParser.ForFunctionContext ctx);
	/**
	 * Enter a parse tree produced by {@link GrammarParser#returnValue}.
	 * @param ctx the parse tree
	 */
	void enterReturnValue(GrammarParser.ReturnValueContext ctx);
	/**
	 * Exit a parse tree produced by {@link GrammarParser#returnValue}.
	 * @param ctx the parse tree
	 */
	void exitReturnValue(GrammarParser.ReturnValueContext ctx);
}