grammar Grammar;

fragment CHARACTER  : [a-zA-Z];
fragment NUM  : [0-9];

WORD : CHARACTER;
NUMBER : NUM;
WHITESPACE : [ \t\r\n] -> skip;

/*entry point*/
entryPoint: (defineVariable | defineFunction | (callFunction SEMICOLON) | whileFunction |
    doWhileFunction | doUntilFunction | ifFunction | untilFunction | forFunction)*;

/*values*/
TRUE : 'true';
FALSE : 'false';
booleanValue : TRUE | FALSE;
number : (MINUS)?NUMBER(NUMBER)*;

/*reocuring tokens*/
identifier : WORD(WORD)*;
OPEN_BRACKET : '(';
CLOSE_BRACKET : ')';
OPEN_FUNC_BRACKET : '{';
CLOSE_FUNC_BRACKET : '}';
COMMA : ',';
SEMICOLON : ';';
QUATE : '"';
QUESTION_MARK : '?';
COLON : ':';

/*type*/
INT   : 'int';
BOOLEAN   : 'boolean';
VOID   : 'void';
dateType : INT | BOOLEAN;
dateTypeFce : dateType | VOID;

/*variable types*/
CREATE_VAR : 'definuj';
CREATE_CONST : 'definuj_konstanta';
create : CREATE_VAR | CREATE_CONST;

/*operators*/
ASSIGN : '=';
PLUS : '+';
MINUS : '-';
MULTIPLY : '*';
DIVIDE : '/';
addOperator : PLUS | MINUS;
multiplyOperator: MULTIPLY | DIVIDE;

/*logical operatory*/
EQUAL : '==';
LESS : '<';
EQ_LESS : '<=';
GREATER : '>';
EQ_GREATER : '>=';
NOT_EQUAL : '!=';
AND : '&&';
OR : '||';
NEGATION : '!';
logicalOperator : EQUAL
    | LESS
    | EQ_LESS
    | GREATER
    | EQ_GREATER
    | NOT_EQUAL;

logicalOperatorBinary : AND
    | OR;

/*operation with values*/
logicalOperation : negateLogicalOperation (logicalOperatorBinary negateLogicalOperation)*;
negateLogicalOperation : (NEGATION)? (logicalOperationInner);
logicalOperationInner: identifier | booleanValue | numeralOperation logicalOperator numeralOperation;
numeralOperation: numeralOperation addOperator numeralOperationInner | numeralOperationInner;
numeralOperationInner: numeralOperationInner multiplyOperator numeralOperationFinal | numeralOperationFinal;
numeralOperationFinal: identifier | number | bracketsOperation;

bracketsOperation : (OPEN_BRACKET numeralOperation CLOSE_BRACKET);
operation : numeralOperation | logicalOperation | callFunction | ternaryOperation;
ternaryOperation : condition QUESTION_MARK operation COLON operation;

/*function tokens*/
FUNCTION_KEYWORD : 'funkce';
parameters : OPEN_BRACKET (parameter (COMMA parameter)*)? CLOSE_BRACKET;
parameter : dateType identifier;

/*cykles and conditions*/
IF_KEYWORD : 'pokud';
WHILE_KEYWORD : 'dokud';
UNTIL_KEYWORD : 'dokudNe';
DO_KEYWORD : 'opakuj';
RETURN_KEYWORD : 'vrat';
ElSE_KEYWORD : 'jinak';
FOR_KEYWORD : 'pro';
condition: OPEN_BRACKET logicalOperation CLOSE_BRACKET;

/*define variable and function*/
defineVariable : create dateType identifier ASSIGN (identifier ASSIGN)* operation SEMICOLON
                | create dateType multipleIdentifiers ASSIGN multipleOperations SEMICOLON;
multipleIdentifiers : OPEN_FUNC_BRACKET identifier (COMMA identifier)+ CLOSE_FUNC_BRACKET;
multipleOperations : OPEN_FUNC_BRACKET operation (COMMA operation)+ CLOSE_FUNC_BRACKET;

defineFunction : FUNCTION_KEYWORD dateTypeFce identifier parameters functionBody;
callFunction : identifier callFunctionParameters;
callFunctionParameters : OPEN_BRACKET ((identifier | booleanValue | number) (COMMA (identifier | booleanValue | number))*)? CLOSE_BRACKET;

/*functions*/
functionBody : OPEN_FUNC_BRACKET (functionRow)* CLOSE_FUNC_BRACKET;
functionRow : defineVariable | ifFunction | whileFunction | untilFunction
    | doWhileFunction | doUntilFunction | returnValue | (callFunction SEMICOLON) | forFunction;

/*if*/
ifFunction : IF_KEYWORD condition functionBody (ElSE_KEYWORD functionBody)?
    | QUESTION_MARK condition functionBody COLON functionBody;

/*cycles*/
whileFunction : WHILE_KEYWORD condition functionBody;
doWhileFunction : DO_KEYWORD functionBody WHILE_KEYWORD condition;
untilFunction : UNTIL_KEYWORD condition functionBody;
doUntilFunction : DO_KEYWORD functionBody UNTIL_KEYWORD condition;
forFunction : FOR_KEYWORD OPEN_BRACKET
        identifier ASSIGN numeralOperation SEMICOLON
        logicalOperation SEMICOLON
        identifier ASSIGN numeralOperation
    CLOSE_BRACKET functionBody;

/*return*/
returnValue : RETURN_KEYWORD (identifier | number | booleanValue)? SEMICOLON;